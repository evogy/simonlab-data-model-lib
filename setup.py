from setuptools import setup

setup(
    name="datamodel",
    version="0.2.1",
    description="SimonLab Data Model",
    url="git@bitbucket.org:evogy/simonlab-data-model-lib.git",
    author="Francesco Prete",
    author_email="francesco.prete@evogy.it",
    license="unlicense",
    packages=["datamodel", "datamodel.db", "datamodel.modeling"],
    install_requires=[
        "pandas",
        "numpy",
        "pyArango",
        "python-dotenv",
        "pytz",
        "PyYAML",
        "requests",
    ],
    zip_safe=False,
)
