# SimonLab Data Model 

## Requirements

### Start ArangoDB server
```
docker run -p 8529:8529 -e ARANGO_ROOT_PASSWORD=openSesame arangodb/arangodb:3.9.0
```

## Installation
Using pip:
```
pip install git+ssh://eldritchhh@bitbucket.org/evogy/simonlab-data-model-lib.git
```

In `requirements.txt`:
```
datamodel @ git+https://eldritchhh@bitbucket.org/evogy/simonlab-data-model-lib.git
```

## Usage: Steps

### Create DB & Push algos
```
python .\datamodel\init_db.py -db "db_name" [--remote]
```
Using the "--remote" flag the new DB will be create on the remote ArangoDb instance. 

### Create datapoint on SimonLab & Update config file
When a new config file based on "template.yml" is ready:

⚠️ "actions" section can be ignored if you don't want to apply control algorithms on this site.

```
python .\datamodel\create_dps.py -cp "config_file_path/name.yml" --update_config
```
Here you can specify 3 tags:
- `--dev`: datapoint will be create in SimonLab Dev; otherwise will be created in SimonLab Prod
- `--create_cmds`: using this flag, "commands" section will be created based on specified equips
- `--create_algo_fbs`: using this flag, for each writable datapoint associated to a command, a readable datapoint will be created

"create_dps.py" script will create a new config file called: "updated_name.yml".

### Update manually config file
Now in "updated_name.yml" the following fields must be updated:
- `output_type` and `output_specs` in `commands` (if used "--create_cmds")
- `schedule_dp` in `comfort_settings`

Is also mandatory to update "season" and "sched_comforts" datapoints on SimonLab.

### Push config file on DB
The final step is to push the config file in the DB:
```
python .\datamodel\push_config_file.py -db "db_name" -cp "config_file_path/update_name.yml" [--remote]
```

### Delete site from DB
If something goes wrong and you want to delete all elements connected to a "site" entity you can run:

```
python .\datamodel\delete_site.py -db "db_name" -s "site_key"
```
⚠️ "site_key" is the "_key" field in the site document stored in ArangoDb.

## Usage: Code snippet

### DB Connection
``` { .python }
from pyArango.connection import Connection

conn = Connection(
    arangoURL="http://localhost:8529", username="root", password="openSesame"
)
conn.createDatabase(name="test_db")
db = conn["test_db"]
```