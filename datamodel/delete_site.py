import os
import argparse
from pyArango.connection import Connection

if __name__ == "__main__":
    from modeling import *
    from dotenv import load_dotenv, find_dotenv

    load_dotenv(find_dotenv())
else:
    from .modeling import *


def delete_site(host, database_name, username, password, site_key):
    conn = Connection(
        arangoURL=host,
        username=username,
        password=password,
    )
    db = conn[database_name]

    site = Site(**db["SpaceCol"].fetchDocument(site_key, rawResults=True))
    site.delete_buildings(db)
    site.to_document(db).delete()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--site_key", type=str)
    parser.add_argument("-db", "--database_name", type=str, default="new_database")
    parser.add_argument("--remote", action="store_true")
    args = parser.parse_args()

    username = "root"
    if args.remote:
        host = os.getenv("REMOTE_ARANGO_BASE_URL")
        password = os.getenv("REMOTE_ARANGO_PSW")
    else:
        host = os.getenv("LOCAL_ARANGO_BASE_URL")
        password = os.getenv("LOCAL_ARANGO_PSW")

    delete_site(host, args.database_name, username, password, args.site_key)
