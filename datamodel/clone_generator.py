import yaml
import argparse
from pprint import pprint
from dotenv import load_dotenv, find_dotenv

from modeling import *


load_dotenv(find_dotenv())


def _get_measurements(measurements, uuid, dps):
    out = []
    if measurements is not None:
        for measurement in measurements:
            dp_type = [dp for dp in dps if dp["uuid"] == measurement][0]["type"]
            out.append(dp_type + "_" + uuid)
    return out


def generate_config_file(config_file):
    params = config_file["params"]
    space_template = config_file["spaces"][0]
    equip_template = config_file["equips"][0]
    datapoints_template = config_file["readable_datapoints"]

    template_children = (
        equip_template["children"] if equip_template.get("children") is not None else []
    )
    template_measurements = (
        equip_template["measurements"]
        if equip_template.get("measurements") is not None
        else []
    )

    new_dps = []
    # new_sensors = []
    for i in range(len(params["names"])):
        print(f"Clone # {i+1}")
        uuid = params["names"][i].lower().replace(" ", "_")
        device_code = (
            params["devices"][i]
            if params.get("devices") is not None
            else equip_template["device_code"]
        )

        # SPACES
        if bool(params.get("duplicate_spaces")):
            config_file["spaces"].append(
                {
                    "uuid": uuid,
                    "name": params["names"][i],
                    "is_in": space_template["is_in"],
                    "type": space_template["type"],
                }
            )
            print(" - spaces")

        # DATAPOINTS
        for datapoint in datapoints_template:
            new_dps.append(
                {
                    "uuid": datapoint["type"] + "_" + uuid,
                    "code": datapoint["code"],
                    "device_code": device_code,
                    "type": datapoint["type"],
                }
            )
        print(" - datapoints")

        # EQUIPS + SENSOR
        equip = {
            "uuid": equip_template["type"] + "_" + uuid,
            "name": equip_template["type"].capitalize() + " " + params["names"][i],
            "type": equip_template["type"],
            "device_code": params["devices"][i]
            if params.get("devices") is not None
            else equip_template["device_code"],
            "serve": uuid
            if bool(params.get("duplicate_spaces"))
            else equip_template["serve"],
        }

        children = []
        for child in template_children:
            children.append(
                {
                    "uuid": child["type"] + "_" + uuid,
                    "type": child["type"],
                    "measurements": _get_measurements(
                        child.get("measurements"),
                        uuid,
                        config_file["readable_datapoints"] + new_dps,
                    ),
                }
            )
        equip["children"] = children
        equip["measurements"] = _get_measurements(
            template_measurements, uuid, config_file["readable_datapoints"] + new_dps
        )

        config_file["equips"].append(equip)
        print(" - equips")

    config_file["readable_datapoints"] = config_file["readable_datapoints"] + new_dps


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-cp", "--config_path", type=str, default="config_files/cloning_template.yml"
    )
    parser.add_argument("--generate_file", action="store_true")
    args = parser.parse_args()

    config_file = load_config_file(args.config_path)

    generate_config_file(config_file)
    _ = config_file.pop("params")

    filename = args.config_path.split("/")[-1]
    path = args.config_path.replace(filename, "updated_" + filename)
    if args.generate_file:
        with open(path, "w") as outfile:
            yaml.dump(config_file, outfile, default_flow_style=False, sort_keys=False)
