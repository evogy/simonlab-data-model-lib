import os
import yaml
import argparse
from dotenv import load_dotenv, find_dotenv

from modeling import *
from ecologiclib import create_device_if_not_exists, create_datapoint_if_not_exists

load_dotenv(find_dotenv())

# TODO: Non mi piace che i nomi dipendano dagli uuid che di sicuro li metteremo a caso


def _choose_cmd_type(equip):
    if "fan" in equip["type"]:
        return "ventilation_control"
    elif "damper" in equip["type"]:
        return "damper_control"
    elif "thermostat" in equip["type"]:
        return "temperature_control"
    else:
        return None


def create_datapoint(
    base_url,
    token,
    plant_code,
    device_code,
    dp_code,
    dp_name,
    writable=True,
    driver=None,
    value_type="numeric",
    enum_dictionary=None,
):
    datapoint_info = {
        "plant_code": plant_code,
        "device_code": device_code,
        "dp_code": dp_code,
        "dp_name": dp_name,
        "dp_tag_id": "5c90c6275b30a5e20425a0f9",
        "dp_value_type": value_type,
        "dp_is_writable": writable,
    }
    if driver is not None:
        datapoint_info["dp_driver"] = driver

    if enum_dictionary is not None:
        datapoint_info["dp_enum_dictionary"] = enum_dictionary

    return create_datapoint_if_not_exists(base_url, token, datapoint_info)


def create_datapoint_and_command(
    base_url,
    token,
    plant_code,
    device_code,
    dp_code,
    dp_name,
    cmd_type,
    equip_ref=None,
    create_cmds=False,
):
    dp = create_datapoint(base_url, token, plant_code, device_code, dp_code, dp_name)

    config_file["writable_datapoints"].append(
        {
            "uuid": dp_code,
            "code": dp_code,
            "device_code": device_code,
            "simonlab_id": dp["id"],
        }
    )
    if create_cmds:
        config_file["commands"].append(
            {
                "name": f"{cmd_type.replace('-', '_')}_{dp_code}",
                "type": cmd_type,
                "implemented_by": equip_ref,
                "output_dp": dp_code,
                "output_type": "",
                "output_specs": "",
            }
        )
    print(f"\t{dp_code} - (id: {dp['id']})")


def create_readable_datapoint(config_file, base_url, token, create_feedbacks):
    print("READABLE DATAPOINT")

    plant_code = config_file["default_parameter"]["plant_code"]

    # CREATE DEVICE "algo"
    _ = create_device_if_not_exists(
        base_url,
        token,
        {
            "plant_code": plant_code,
            "device_code": "algo",
            "device_name": "Algoritmo di Controllo",
        },
    )
    print("DEVICE: 'algo'")

    # CREATE "watchdog_read"
    watchdog_read = create_datapoint(
        base_url,
        token,
        plant_code,
        "algo",
        "watchdog",
        "Watchdog - Read",
        writable=False,
    )
    print(f"\twatchdog_read - (id: {watchdog_read['id']})")

    if create_feedbacks:
        print("\n")
        for equip in config_file["equips"]:
            print(f"DEVICE: '{equip['device_code']}'")
            for subequip in equip["children"]:
                dp = create_datapoint(
                    base_url=base_url,
                    token=token,
                    plant_code=plant_code,
                    device_code=equip["device_code"],
                    dp_code=subequip["uuid"] + "_feedback",
                    dp_name=f"Feedback algo - {subequip['uuid'].replace('_', ' ').capitalize()}",
                    writable=False,
                )
                print(f"\t{dp['code']} - (id: {dp['id']})")
            print("\n")


def create_writable_datapoints(
    config_file, base_url, token, create_commands, create_operation_modes
):
    print("\n\nWRITABLE DATAPOINT")

    plant_code = config_file["default_parameter"]["plant_code"]
    config_file["writable_datapoints"] = []
    #     config_file["writable_datapoints"]
    #     if config_file.get("writable_datapoints") is not None
    #     else []
    # )
    if create_commands:
        config_file["commands"] = (
            config_file["commands"] if config_file.get("commands") is not None else []
        )
    site = [space for space in config_file["spaces"] if space["type"] == "site"][0]
    rooms = [space for space in config_file["spaces"] if space["type"] == "room"]

    # GET DEVICE "algo"
    _ = create_device_if_not_exists(
        base_url,
        token,
        {
            "plant_code": plant_code,
            "device_code": "algo",
            "device_name": "Algoritmo di Controllo",
        },
    )
    print("DEVICE: 'algo'")

    # CREATE "watchdog"
    watchdog = create_datapoint(
        base_url, token, plant_code, "algo", "watchdog", "Watchdog"
    )
    config_file["writable_datapoints"].append(
        {
            "uuid": "watchdog",
            "device_code": "algo",
            "code": "watchdog",
            "simonlab_id": watchdog["id"],
        }
    )
    site["watchdog_dp"] = "watchdog"
    print("\twatchdog")

    # CREATE "season"
    enum_dictionary = [
        {"key": "0", "value": "INVERNO"},
        {"key": "1", "value": "ESTATE"},
    ]
    season = create_datapoint(
        base_url,
        token,
        plant_code,
        "algo",
        "season",
        "Stagione",
        value_type="enum",
        enum_dictionary=enum_dictionary,
    )
    config_file["writable_datapoints"].append(
        {
            "uuid": "season",
            "device_code": "algo",
            "code": "season",
            "simonlab_id": season["id"],
        }
    )
    site["season_dp"] = "season"
    print("\tseason")

    # CREATE "sched_comfort"
    for cs in config_file["comfort_settings"]:
        sched_name = (
            cs["schedule_dp"]
            .replace("_", " ")
            .replace("sched comfort ", "")
            .capitalize()
        )
        sched_comfort = create_datapoint(
            base_url,
            token,
            plant_code,
            "algo",
            cs["schedule_dp"],
            f"Schedulazioni Comfort - {sched_name}",
        )
        dps = [
            dp
            for dp in config_file["writable_datapoints"]
            if dp["uuid"] == cs["schedule_dp"]
        ]
        if len(dps) == 0:
            config_file["writable_datapoints"].append(
                {
                    "uuid": cs["schedule_dp"],
                    "device_code": "algo",
                    "code": cs["schedule_dp"],
                    "simonlab_id": sched_comfort["id"],
                }
            )
            print(f'\t{cs["schedule_dp"]}')

    # CREATE "operation_mode"
    if create_operation_modes:
        enum_dictionary = [
            {"key": "0", "value": "ALGORITMO"},
            {"key": "1", "value": "MANUALE"},
        ]
        for room in rooms:
            _ = create_datapoint(
                base_url,
                token,
                plant_code,
                "algo",
                f"operation_mode_{room['uuid']}",
                f"Set modalità Manuale / Automatica - {room['name'].lower().capitalize()}",
                value_type="enum",
                enum_dictionary=enum_dictionary,
            )
            print(f"\toperation_mode_{room['uuid']}")
    print("\n")

    if create_commands:
        for equip in config_file["equips"]:
            print(f"DEVICE: '{equip['device_code']}'")

            _ = create_device_if_not_exists(
                base_url,
                token,
                {
                    "plant_code": plant_code,
                    "device_code": equip["device_code"],
                    "device_name": equip["device_code"],
                },
            )

            # CREATE ON-OFF
            create_datapoint_and_command(
                base_url=base_url,
                token=token,
                plant_code=plant_code,
                device_code=equip["device_code"],
                dp_code=equip["uuid"] + "_cmd",
                dp_name=f"Cmd algo - {equip['uuid'].replace('_', ' ').capitalize()}",
                cmd_type="on-off",
                equip_ref=equip["uuid"],
                create_cmds=create_commands,
            )

            for subequip in equip["children"]:
                # CREATE SUB-EQUIP CMD
                create_datapoint_and_command(
                    base_url=base_url,
                    token=token,
                    plant_code=plant_code,
                    device_code=equip["device_code"],
                    dp_code=subequip["uuid"] + "_cmd",
                    dp_name=f"Cmd algo - {subequip['uuid'].replace('_', ' ').capitalize()}",
                    equip_ref=subequip["uuid"],
                    cmd_type=_choose_cmd_type(subequip),
                    create_cmds=create_commands,
                )
            print("\n")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-cp", "--config_path", type=str, default="config_files/mot.yml"
    )
    parser.add_argument("--dev", action="store_true")
    parser.add_argument("--update_config", action="store_true")
    parser.add_argument("-f", "--create_feedbacks", action="store_true")
    parser.add_argument("-c", "--create_commands", action="store_true")
    parser.add_argument("-op", "--create_operation_modes", action="store_true")
    args = parser.parse_args()

    if args.dev:
        SIMONLAB_BASE_URL = os.getenv("SIMONLAB_BASE_URL_DEV")
        SIMONLAB_TOKEN = os.getenv("SIMONLAB_TOKEN_DEV")
    else:
        SIMONLAB_BASE_URL = os.getenv("SIMONLAB_BASE_URL")
        SIMONLAB_TOKEN = os.getenv("SIMONLAB_TOKEN")

    config_file = load_config_file(args.config_path)

    create_readable_datapoint(
        config_file, SIMONLAB_BASE_URL, SIMONLAB_TOKEN, args.create_feedbacks
    )
    create_writable_datapoints(
        config_file,
        SIMONLAB_BASE_URL,
        SIMONLAB_TOKEN,
        args.create_commands,
        args.create_operation_modes,
    )

    # filename = args.config_path.split("/")[-1]
    # path = args.config_path.replace(filename, "updated_" + filename)
    if args.update_config:
        with open(args.config_path, "w") as outfile:
            yaml.dump(config_file, outfile, default_flow_style=False, sort_keys=False)
