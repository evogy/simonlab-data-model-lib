import os
import requests
import numpy as np
import pandas as pd
from datetime import date, datetime, timedelta
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())

DATA_BASE_URL = os.getenv("DATA_BASE_URL")
DATA_TOKEN = os.getenv("DATA_TOKEN")

SIMONLAB_BASE_URL = os.getenv("SIMONLAB_BASE_URL")
SIMONLAB_TOKEN = os.getenv("SIMONLAB_TOKEN")


# ACTION TYPE: "on-off"


def sleep(db, **kwargs):
    return False


def schedule_based_startup(db, **kwargs):
    return True


# NB. questo coincide con la prima occorrenza di "sleep"
# def schedule_based_shutdown(db, equip, equip_data):
#     return 0


def optimal_startup(db, equip, base_url, token, season, **kwargs):
    # if equip.is_off(db, data=equip_data):
    if equip.is_off(db, base_url, token):
        return not equip.check_comfort(db, base_url, token, season, "red")


def optimal_shutdown(db, equip, base_url, token, season, **kwargs):
    if equip.is_on(db, base_url, token):
        return equip.check_comfort(db, base_url, token, season, "green")


# ACTION TYPE: "damper_control"


def min_air_damper_control(db, equip, season, base_url, token, **kwargs):
    parent_equip = equip.get_parent_equip(db)
    if parent_equip.is_on(db, base_url, token):
        cs = equip.get_space(db).get_comfort_settings(db, attribute="co2")
        th = cs.get_params(season=season)["max"] - 300

        date_from = datetime.utcnow() - timedelta(hours=1)
        data = parent_equip.get_sensors_data_by_attribute(
            db,
            base_url,
            token,
            date_from=date_from,
            attribute="co2",
            aggregation="mean",
        )
        if data is None:
            return

        return data.tail(1)["co2"].item() > th


def free_cooling_heating_damper_outer_temperature_control(
    db, equip, season, outer_temperature, base_url, token, **kwargs
):
    parent_equip = equip.get_parent_equip(db)
    if parent_equip.is_on(db, base_url, token):
        actual_value = outer_temperature.tail(1)["outer_temperature"].item()
        upper_limit, lower_limit = get_limits(
            db, equip, season, "temperature", offset=1
        )
        return lower_limit <= actual_value <= upper_limit


def free_cooling_heating_damper_co2_control(
    db, equip, season, base_url, token, space_custom_settings, **kwargs
):
    def predict_co2(data, col, now, settings):
        profile_idx = str(_get_profile_idx(data, col, settings))
        if profile_idx is None:
            return

        profile = pd.read_csv(settings["profiles"]).get(profile_idx)
        idx = int(now.hour * 4) + int(now.minute / 15)
        delta = profile.iloc[idx + 1] - profile.iloc[idx]
        actual_co2 = data.tail(1).get(col).item()

        return actual_co2 * (100 + (delta / profile[idx] * 100)) / 100, profile

    parent_equip = equip.get_parent_equip(db)
    if parent_equip.is_on(db, base_url, token):
        cs = equip.get_space(db).get_comfort_settings(db, attribute="co2")
        th = cs.get_params(season=season)["max"]

        date_to = datetime.utcnow()
        date_from = date_to - timedelta(hours=24)
        data = parent_equip.get_sensors_data_by_attribute(
            db,
            base_url,
            token,
            date_from=date_from,
            attribute="co2",
            aggregation="mean",
        )
        if data is None:
            return

        settings = space_custom_settings["algorithms"][
            "free_cooling_heating_damper_co2_control"
        ]
        actual_co2 = data.tail(1)["co2"].item()
        pred_co2, _ = predict_co2(data, "co2", date_to, settings)
        return (actual_co2 > th) or (pred_co2 > th if pred_co2 is not None else True)


# ACTION TYPE: "ventilation_control"


def fan_outer_temperature_control(
    db, equip, season, outer_temperature, base_url, token, **kwargs
):
    MAX = 1.0
    MIN = 0.0

    def _compute(x, l, u, min_val=MIN, max_val=MAX):
        return (
            (max_val * (x - 0.5 * (l + u)) * (x - u)) / ((l - 0.5 * (l + u)) * (l - u))
            + (min_val * (x - l) * (x - u))
            / ((0.5 * (l + u) - l) * (0.5 * (l + u) - u))
            + (max_val * (x - l) * (x - 0.5 * (l + u)))
            / ((u - l) * (u - 0.5 * (l + u)))
        )

    parent_equip = equip.get_parent_equip(db)
    if parent_equip.is_on(db, base_url, token):
        actual_value = outer_temperature.tail(1)["outer_temperature"].item()
        lower_limit, upper_limit = get_limits(
            db, equip, season, "temperature", offset=1
        )

        if actual_value <= lower_limit:
            return MAX
        elif lower_limit < actual_value < upper_limit:
            return _compute(actual_value, lower_limit, upper_limit)
        elif actual_value >= upper_limit:
            return MAX


def fan_supply_air_temperature_control(db, equip, season, base_url, token, **kwargs):
    parent_equip = equip.get_parent_equip(db)
    if parent_equip.is_on(db, base_url, token):
        date_to = datetime.utcnow()
        date_from = date_to - timedelta(hours=1)
        data = equip.get_sensors_data_by_attribute(
            db,
            base_url,
            token,
            date_from=date_from,
            attribute="supply_air_temperature",
            aggregation="mean",
        )
        if data is None:
            return

        actual_value = data.tail(1)["supply_air_temperature"].item()
        if season == "summer":
            return min(max((1 - (actual_value - 15.0) / (35.0 - 15.0)), 0.0), 1.0)
        else:
            return min(max((actual_value - 15.0) / (35.0 - 15.0), 0.0), 1.0)


def fan_inner_temperature_control(db, equip, season, base_url, token, **kwargs):
    parent_equip = equip.get_parent_equip(db)
    if parent_equip.is_on(db, base_url, token):
        upper_limit, lower_limit = get_limits(
            db, equip, season, "temperature", color="yellow"
        )
        delta_limit = upper_limit - lower_limit

        date_to = datetime.utcnow()
        date_from = date_to - timedelta(hours=1)
        data = parent_equip.get_sensors_data_by_attribute(
            db,
            base_url,
            token,
            date_from=date_from,
            attribute="temperature",
            aggregation="mean",
        )
        if data is None:
            return

        actual_value = data.tail(1)["temperature"].item()
        if season == "summer":
            return min(max((actual_value - lower_limit) / delta_limit, 0.0), 1.0)
        else:
            return min(max((1 - (actual_value - lower_limit) / delta_limit), 0.0), 1.0)


def fan_co2_control(
    db, equip, season, base_url, token, space_custom_settings, **kwargs
):
    parent_equip = equip.get_parent_equip(db)
    if parent_equip.is_on(db, base_url, token):
        date_to = datetime.utcnow()
        date_from = date_to - timedelta(hours=24)
        # TODO: QUI DEVO METTERE PARENT EQUIP / EQUIP IN BASE A DOVE DECIDO DI METTERE
        # CO2. QUESTO VALE PER TUTTO IL COMFORT (CHE PER STANDARD POTREBBE ESSERE ASSEGNATO)
        # AD UN SENSOR DELL'EQUIP PADRE.
        data = parent_equip.get_sensors_data_by_attribute(
            db,
            base_url,
            token,
            date_from=date_from,
            date_to=date_to,
            attribute="co2",
            aggregation="mean",
        )
        if data is None:
            return

        settings = space_custom_settings["algorithms"]["fan_co2_control"]

        profile_idx = str(_get_profile_idx(data, "co2", settings))
        if profile_idx is None:
            return

        profile = pd.read_csv(settings["profiles"]).get(profile_idx)
        idx = int(date_to.hour * 4) + int(date_to.minute / 15)
        delta = profile.iloc[idx + 1] - profile.iloc[idx]

        diff_profile = profile.diff()
        diff_profile = diff_profile.rolling(7, center=True).mean().ffill().bfill()

        upper_limit, lower_limit = get_limits(
            db, equip, season, "temperature", color="yellow"
        )
        perc = (profile[idx + 1] - lower_limit) / (upper_limit - lower_limit)

        diff_perc_5 = np.percentile(diff_profile, 5)
        diff_perc_95 = np.percentile(diff_profile, 95)
        diff_perc = (delta + abs(diff_perc_5)) / (diff_perc_95 + abs(diff_perc_5))
        return min(max(0.25 * diff_perc + 0.75 * perc, 0.0), 1.0)


# ACTION TYPE: "temperature_control"
def fixed_temperature_setpoint_control(db, **kwargs):
    return


# UTILS


def get_limits(db, equip, season, attribute="temperature", color=None, offset=None):
    cs = equip.get_space(db).get_comfort_settings(db, attribute=attribute)
    params = cs.get_params(season=season)
    if color is not None:
        if color == "red":
            lower_limit, upper_limit = get_red_limit(params)
        elif color == "green":
            lower_limit, upper_limit = get_green_limit(params)
        else:
            lower_limit, upper_limit = get_yellow_limit(params)

    if offset is not None:
        upper_limit = params["max"] + offset
        lower_limit = params["min"] - offset

    return lower_limit, upper_limit


def get_green_limit(comfort_params):
    return (
        float(comfort_params["min"]) + float(comfort_params["tol"]),
        float(comfort_params["max"]) - float(comfort_params["tol"]),
    )


def get_yellow_limit(comfort_params):
    return (
        float(comfort_params["min"]),
        float(comfort_params["max"]),
    )


def get_red_limit(comfort_params):
    return (
        float(comfort_params["min"]) - float(comfort_params["tol"]),
        float(comfort_params["max"]) + float(comfort_params["tol"]),
    )


def _get_profile_idx(data, col, settings):
    json_data = data[[col]].copy()
    json_data = json_data[json_data.index.day >= date.today().day]
    json_data = json_data.reset_index(drop=True).transpose().to_dict(orient="split")

    req = requests.post(
        settings["base_url"],
        headers={"Authorization": f"Bearer {settings['token']}"},
        json=json_data,
    )
    if req.status_code == 200:
        return int(req.json()[0])
