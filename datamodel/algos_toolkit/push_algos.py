import argparse
from pyArango.connection import Connection


def push_algos(host, database_name, username, password):
    conn = Connection(
        arangoURL=host,
        username=username,
        password=password,
    )
    db = conn[database_name]
    coll = db["AlgorithmCol"]

    sleep = coll.createDocument(
        {
            "uuid": "sleep",
            "name": "sleep",
            "function_name": "sleep",
        }
    )
    sleep.save()
    schedule_based_startup = coll.createDocument(
        {
            "uuid": "schedule_based_startup",
            "name": "schedule_based_startup",
            "function_name": "schedule_based_startup",
        }
    )
    schedule_based_startup.save()
    optimal_startup = coll.createDocument(
        {
            "uuid": "optimal_startup",
            "name": "optimal_startup",
            "function_name": "optimal_startup",
        }
    )
    optimal_startup.save()
    optimal_shutdown = coll.createDocument(
        {
            "uuid": "optimal_shutdown",
            "name": "optimal_shutdown",
            "function_name": "optimal_shutdown",
        }
    )
    optimal_shutdown.save()

    # DAMPER
    min_air_damper_control = coll.createDocument(
        {
            "uuid": "min_air_damper_control",
            "name": "min_air_damper_control",
            "function_name": "min_air_damper_control",
        }
    )
    min_air_damper_control.save()
    free_cooling_heating_damper_outer_temperature_control = coll.createDocument(
        {
            "uuid": "free_cooling_heating_damper_outer_temperature_control",
            "name": "free_cooling_heating_damper_outer_temperature_control",
            "function_name": "free_cooling_heating_damper_outer_temperature_control",
        }
    )
    free_cooling_heating_damper_outer_temperature_control.save()
    free_cooling_heating_damper_co2_control = coll.createDocument(
        {
            "uuid": "free_cooling_heating_damper_co2_control",
            "name": "free_cooling_heating_damper_co2_control",
            "function_name": "free_cooling_heating_damper_co2_control",
        }
    )
    free_cooling_heating_damper_co2_control.save()

    # FAN
    fan_outer_temperature_control = coll.createDocument(
        {
            "uuid": "fan_outer_temperature_control",
            "name": "fan_outer_temperature_control",
            "function_name": "fan_outer_temperature_control",
        }
    )
    fan_outer_temperature_control.save()
    fan_supply_air_temperature_control = coll.createDocument(
        {
            "uuid": "fan_supply_air_temperature_control",
            "name": "fan_supply_air_temperature_control",
            "function_name": "fan_supply_air_temperature_control",
        }
    )
    fan_supply_air_temperature_control.save()
    fan_inner_temperature_control = coll.createDocument(
        {
            "uuid": "fan_inner_temperature_control",
            "name": "fan_inner_temperature_control",
            "function_name": "fan_inner_temperature_control",
        }
    )
    fan_inner_temperature_control.save()
    fan_co2_control = coll.createDocument(
        {
            "uuid": "fan_co2_control",
            "name": "fan_co2_control",
            "function_name": "fan_co2_control",
        }
    )
    fan_co2_control.save()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-db", "--database_name", type=str, default="new_database")
    parser.add_argument("-u", "--username", type=str, default="root")
    parser.add_argument("-p", "--password", type=str, default="openSesame")
    parser.add_argument("--host", type=str, default="http://localhost:8529")
    args = parser.parse_args()

    push_algos(args.host, args.database_name, args.username, args.password)
