DEFAULT_VALIDATION = {
    "on_save": True,
    "on_set": True,
    "on_load": True,
    "allow_foreign_fields": False,
}

PARENT_EQUIPS = [
    "heat_pump",
    "uta",
    "vrf",
    "fancoil",
    "rooftop",
]

CHILD_EQUIPS = [
    "thermostat",
    "rh_sensor",
    "supply_air_fan",
    "return_air_fan",
    "outer_air_damper",
    "inner_air_damper",
    "valve",
    "pump"
]

EQUIPS = PARENT_EQUIPS + CHILD_EQUIPS

SPACES = ["area", "room", "floor", "building", "site"]

PERIODS = ["eco1", "in-sched", "eco2", "off-sched"]

ACTIONS = [
    # "sleep",
    # "startup",
    # "shutdown",
    "on-off",
    "damper_control",
    "ventilation_control",
    "temperature_control",
]
ALGO_AGGREGATIONS = ["mean", "weighted_mean", "max", "min", "and", "or"]

CONTROL_DATAPOINT_TYPE = ["season", "schedule", "watchdog", "outer_temperature"]
COMFORT_DATAPOINT_TYPES = ["co2", "temperature", "humidity"]
SAVING_DATAPOINT_TYPES = ["energy", "power", "baseline"]
EQUIP_DATAPOINT_TYPES = [
    "on-off",
    "temperature_setpoint",
    "co2_setpoint",
    "humidity_setpoint",
    "supply_air_temperature",
    "supply_water_temperature",
    "supply_water_setpoint",
    "supply_air_humidity",
    "return_air_temperature",
    "return_air_humidity",
    "return_water_temperature",
    "supply_fan_flow_rate",  # -> modulabile
    "return_fan_flow_rate",  # -> modulabile
    "supply_fan_state",  # -> booleana
    "return_fan_state",  # -> booleana
    "damper_state",  # -> boolean
    "damper_perc",  # -> modulabilea
    "valve_state",
    "pump_state",
    "cooling_state"
]
