import pyArango.collection as COL

# import pyArango.validation as VAL
from .settings import DEFAULT_VALIDATION  # , COMPONENT_OF_TYPES


class IsInCol(COL.Edges):

    _validation = DEFAULT_VALIDATION


class MonitorCol(COL.Edges):

    _validation = DEFAULT_VALIDATION


class ReadCol(COL.Edges):

    _validation = DEFAULT_VALIDATION


class ServeCol(COL.Edges):

    _validation = DEFAULT_VALIDATION


class ConstraintOfCol(COL.Edges):

    _validation = DEFAULT_VALIDATION


class ComponentOfCol(COL.Edges):

    _validation = DEFAULT_VALIDATION

    # _fields = {
    #     "type": COL.Field(validators=[VAL.Enumeration(COMPONENT_OF_TYPES)]),
    # }


class ImplementCol(COL.Edges):

    _validation = DEFAULT_VALIDATION


class PerformCol(COL.Edges):

    _validation = DEFAULT_VALIDATION


class DriveCol(COL.Edges):

    _validation = DEFAULT_VALIDATION
