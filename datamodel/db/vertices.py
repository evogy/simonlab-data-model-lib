import pyArango.collection as COL
import pyArango.validation as VAL

from .settings import (
    DEFAULT_VALIDATION,
    PERIODS,
    EQUIPS,
    SPACES,
    ACTIONS,
    ALGO_AGGREGATIONS,
    CONTROL_DATAPOINT_TYPE,
    COMFORT_DATAPOINT_TYPES,
    SAVING_DATAPOINT_TYPES,
    EQUIP_DATAPOINT_TYPES,
)
from .custom_validators import (
    IdValidator,
    SpaceCustomSettingsValidator,
    ComfortSettingsParamsValidator,
    ActionWhenValidator,
    # EquipSpecsValidator,
)


class ReadableDatapointCol(COL.Collection):

    _validation = DEFAULT_VALIDATION

    _fields = {
        "name": COL.Field(),
        "plant_code": COL.Field(validators=[VAL.String()]),
        "device_code": COL.Field(validators=[VAL.String()]),
        "code": COL.Field(validators=[VAL.String()]),
        "frequency": COL.Field(validators=[VAL.String()]),
        "grouping_function": COL.Field(validators=[VAL.Enumeration(["sum", "mean"])]),
        "type": COL.Field(
            validators=[
                VAL.Enumeration(
                    CONTROL_DATAPOINT_TYPE
                    + COMFORT_DATAPOINT_TYPES
                    + SAVING_DATAPOINT_TYPES
                    + EQUIP_DATAPOINT_TYPES
                )
            ]
        ),
    }


class WritableDatapointCol(COL.Collection):

    _validation = DEFAULT_VALIDATION

    _fields = {
        "name": COL.Field(),
        "plant_code": COL.Field(validators=[VAL.String()]),
        "device_code": COL.Field(validators=[VAL.String()]),
        "code": COL.Field(validators=[VAL.String()]),
        "simonlab_id": COL.Field(validators=[VAL.String()]),
    }


class SensorCol(COL.Collection):

    _validation = DEFAULT_VALIDATION

    _fields = {
        "name": COL.Field(validators=[VAL.String()]),
        "measurements": COL.Field(validators=[VAL.Length(1, 100)]),
    }


class CommandCol(COL.Collection):

    _validation = DEFAULT_VALIDATION

    _fields = {
        "name": COL.Field(),
        "type": COL.Field(validators=[VAL.Enumeration(ACTIONS)]),
        "output_dp": COL.Field(validators=[IdValidator("WritableDatapoint")]),
        "output_type": COL.Field(
            validators=[VAL.Enumeration(["bool", "range", "numeric"])]
        ),
        "output_specs": COL.Field(),
        "duration": COL.Field(validators=[VAL.Int()]),
    }


class ComfortSettingsCol(COL.Collection):

    _validation = DEFAULT_VALIDATION

    _fields = {
        "name": COL.Field(validators=[VAL.String()]),
        "type": COL.Field(validators=[VAL.Enumeration(COMFORT_DATAPOINT_TYPES)]),
        "params_summer": COL.Field(validators=[ComfortSettingsParamsValidator()]),
        "params_winter": COL.Field(validators=[ComfortSettingsParamsValidator()]),
        "schedule_dp": COL.Field(validators=[IdValidator("WritableDatapoint")]),
    }


class EquipCol(COL.Collection):

    validation = DEFAULT_VALIDATION
    validation["on_load"] = False
    validation["allow_foreign_fields"] = True
    _validation = validation

    _fields = {
        "name": COL.Field(validators=[VAL.String()]),
        "type": COL.Field(validators=[VAL.Enumeration(EQUIPS)]),
        "device_code": COL.Field(validators=[VAL.String()]),
        # "schedule_dp": COL.Field(validators=[IdValidator("WritableDatapoint")]),
        # "specs": COL.Field(validators=[EquipSpecsValidator()]),
    }


class SpaceCol(COL.Collection):

    validation = DEFAULT_VALIDATION
    validation["on_load"] = False
    validation["allow_foreign_fields"] = True
    _validation = validation

    _fields = {
        "name": COL.Field(validators=[VAL.String()]),
        "type": COL.Field(validators=[VAL.Enumeration(SPACES)]),
        "custom_settings": COL.Field(validators=[SpaceCustomSettingsValidator()]),
    }


class ActionCol(COL.Collection):

    _validation = DEFAULT_VALIDATION

    _fields = {
        "name": COL.Field(validators=[VAL.String()]),
        "type": COL.Field(validators=[VAL.Enumeration(ACTIONS)]),
        "algo_aggregation": COL.Field(validators=[VAL.Enumeration(ALGO_AGGREGATIONS)]),
        # "equip_aggregation": COL.Field(validators=[VAL.Enumeration(AGGREGATIONS)]),
        # TODO: SERVE UN ULTERIORE PARAMETRO PER INDICARE SE USARE UN EQUIP ALLA VOLTA
        # OPPURE SE SERVE VALUTARLI TUTTI INSIEME (es. load balancing)
        # "": COL.Field(validators=[VAL.Enumeration(["one_equip", "all_equips"])]),
        "when": COL.Field(validators=[ActionWhenValidator(PERIODS)]),
    }


class AlgorithmCol(COL.Collection):

    _validation = DEFAULT_VALIDATION

    _fields = {
        "uuid": COL.Field(validators=[VAL.String()]),
        "name": COL.Field(validators=[VAL.String()]),
        "function_name": COL.Field(validators=[VAL.String()]),
        # "priority": COL.Field(validators=[VAL.Range(0, 9)]),
        "priority": COL.Field(),
    }
