import re
import pyArango.validation as VAL
from pyArango.theExceptions import ValidationError


from .settings import PERIODS


class IdValidator(VAL.Validator):
    def validate(self, value, cls="WritableDatapoint"):
        if type(value) is not str:
            raise ValidationError("Field 'value' must be a string")

        regex = fr"{cls}/[0-9]*"
        if bool(re.search(regex, value)):
            raise ValidationError("Field value must be an ID")
        return True


class SpaceCustomSettingsValidator(VAL.Validator):
    def validate(self, value):
        if value is None:
            return True

        if type(value) is not dict:
            raise ValidationError("Field value must be a dict")
        return True


class ComfortSettingsParamsValidator(VAL.Validator):
    def compare(self, input_dict, input_list):
        if (set(input_dict.keys()) & set(input_list)) != set(input_list):
            return False
        return True

    def validate(self, value):
        if type(value) is not dict:
            raise ValidationError("Field value must be a dict")

        if not (self.compare(value, ["min", "max", "tol"])):
            raise ValidationError(
                f"Keys must be and 'min', 'max', 'tol': found {value.keys()}"
            )
        return True


class ActionWhenValidator(VAL.Validator):
    def validate(self, value, periods=PERIODS):
        if type(value) is not list:
            raise ValidationError("Field 'value' must be a list")

        for element in value:
            if element not in periods:
                raise ValidationError(
                    f"Field value must be in {periods}: found {element}"
                )
        return True


# class EquipSpecsValidator(VAL.Validator):
#     def validate(self, value):
#         if value is None:
#             return True

#         if type(value) is not dict:
#             raise ValidationError("Field value must be a dict")

#         # TODO: fare dei controlli sulle keys
#         return True
