import pyArango.graph as GRAPH


class SimonlabGraph(GRAPH.Graph):
    _edgeDefinitions = [
        GRAPH.EdgeDefinition(
            "IsInCol", fromCollections=["SpaceCol"], toCollections=["SpaceCol"]
        ),
        GRAPH.EdgeDefinition(
            "MonitorCol", fromCollections=["SensorCol"], toCollections=["SpaceCol"]
        ),
        GRAPH.EdgeDefinition(
            "ReadCol", fromCollections=["SensorCol"], toCollections=["EquipCol"]
        ),
        GRAPH.EdgeDefinition(
            "ServeCol", fromCollections=["EquipCol"], toCollections=["SpaceCol", "EquipCol"]
        ),
        GRAPH.EdgeDefinition(
            "ImplementCol", fromCollections=["EquipCol"], toCollections=["CommandCol"]
        ),
        GRAPH.EdgeDefinition(
            "PerformCol", fromCollections=["EquipCol"], toCollections=["ActionCol"]
        ),
        GRAPH.EdgeDefinition(
            "DriveCol", fromCollections=["AlgorithmCol"], toCollections=["ActionCol"]
        ),
        GRAPH.EdgeDefinition(
            "ConstraintOfCol",
            fromCollections=["ComfortSettingsCol"],
            toCollections=["SpaceCol"],
        ),
        GRAPH.EdgeDefinition(
            "ComponentOfCol",
            fromCollections=["EquipCol"],
            toCollections=["EquipCol"],
        ),
    ]
    _orphanedCollections = []
