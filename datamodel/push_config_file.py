import logging
import os
import argparse
from pyArango.connection import Connection

if __name__ == "__main__":
    from modeling import *
    from dotenv import load_dotenv, find_dotenv

    load_dotenv(find_dotenv())
else:
    from .modeling import *


def push_config_file(
    host, database_name, username, password, config_path, base_url, token
):
    conn = Connection(
        arangoURL=host,
        username=username,
        password=password,
    )
    db = conn[database_name]

    config_file = load_config_file(config_path)

    validate_config_file(config_file)

    frequency = "15T"
    plant_code = None
    wdps, rdps, sensors, comforts, equips = ({} for _ in range(5))
    spaces, actions, algos, commands, is_in = ({} for _ in range(5))
    read, serve, monitor, constraint_of, component_of = ({} for _ in range(5))
    drive, perform, implement = ({} for _ in range(3))

    try:
        if config_file.get("default_parameter") is not None:
            if config_file["default_parameter"].get("plant_code") is not None:
                plant_code = str(config_file["default_parameter"]["plant_code"])
            if config_file["default_parameter"].get("frequency") is not None:
                frequency = str(config_file["default_parameter"]["frequency"])

        wdps = {
            dp.get("uuid"): push_writable_datapoint(
                db, dp, plant_code, base_url, token, dp.pop("uuid")
            )
            for dp in config_file.get("writable_datapoints", {})
        }
        print(" - wdps")

        rdps = {
            dp.get("uuid"): push_readable_datapoint(
                db, dp, plant_code, frequency, dp.pop("uuid")
            )
            for dp in config_file.get("readable_datapoints", {})
        }
        print(" - rdps")

        spaces, is_in = push_spaces(db, wdps, rdps, config_file.get("spaces", {}))
        print(" - space")
        print(" - is_in")

        comforts, constraint_of = push_comfort_settings(
            db, wdps, spaces, config_file.get("comfort_settings", {})
        )
        print(" - comforts")
        print(" - constraint_of")

        equips, constraint_of, serve = push_equips(
            db, wdps, rdps, spaces, config_file.get("equips", {})
        )
        print(" - equips")
        print(" - constraint_of")
        print(" - serve")

        sensors, monitor, read = push_sensors(
            db, rdps, spaces, equips, config_file.get("sensors", {})
        )
        print(" - sensors")
        print(" - monitor")
        print(" - read")

        commands, implement = push_commands(
            db, wdps, equips, config_file.get("commands", {})
        )
        print(" - commands")
        print(" - implement")

        actions, drive, perform = push_actions(
            db, equips, config_file.get("actions", {})
        )
        print(" - actions")
        print(" - drive")
        print(" - perform")

    except Exception as e:
        logging.exception(e)
        print("\nDeleting objects from db...")

        [sensors[key].to_document(db).delete() for key in sensors.keys()]
        [comforts[key].to_document(db).delete() for key in comforts.keys()]
        [equips[key].to_document(db).delete() for key in equips.keys()]
        [spaces[key].to_document(db).delete() for key in spaces.keys()]
        [actions[key].to_document(db).delete() for key in actions.keys()]
        [commands[key].to_document(db).delete() for key in commands.keys()]

        [edge.to_document(db).delete() for edge in is_in]
        [edge.to_document(db).delete() for edge in read]
        [edge.to_document(db).delete() for edge in constraint_of]
        [edge.to_document(db).delete() for edge in component_of]
        [edge.to_document(db).delete() for edge in serve]
        [edge.to_document(db).delete() for edge in monitor]
        [edge.to_document(db).delete() for edge in implement]
        [edge.to_document(db).delete() for edge in perform]
        [edge.to_document(db).delete() for edge in drive]

        print("Done!")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-cp", "--config_path", type=str, default="config_files/mot.yml"
    )
    parser.add_argument("-db", "--database_name", type=str, default="new_database")
    parser.add_argument("--remote", action="store_true")
    args = parser.parse_args()

    base_url = os.getenv("SIMONLAB_BASE_URL")
    token = os.getenv("SIMONLAB_TOKEN")

    username = "root"
    if args.remote:
        host = os.getenv("REMOTE_ARANGO_BASE_URL")
        password = os.getenv("REMOTE_ARANGO_PSW")
    else:
        host = os.getenv("LOCAL_ARANGO_BASE_URL")
        password = os.getenv("LOCAL_ARANGO_PSW")

    push_config_file(
        host, args.database_name, username, password, args.config_path, base_url, token
    )
