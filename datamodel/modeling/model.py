import calendar
from tkinter import N
import numpy as np
import pandas as pd
from urllib.parse import urljoin
from datetime import datetime, timedelta

from .utils import (
    _retrieve_data,
    _clean_data,
    get_schedulation,
    send_command_to_simon,
    read_writable_datapoint,
)

try:
    from ..db import *
except:
    from db import *
    import algos_toolkit


class BaseObject:
    def __init__(self, _id, _key, _rev):
        self._id = _id
        self._key = _key
        self._rev = _rev

    def _update_privates(self, doc):
        self._id = doc._id
        self._key = doc._key
        self._rev = doc._rev

    def to_document(self, db):
        # Se con la classe non funziona, prendo il "parent".
        # Ad oggi serve solo per "Site" ma mi sembra una soluzione più generale
        # rispetto a dire ' if(type(self) == "Site") '
        try:
            colname = f"{type(self).__name__}Col"
            return db[colname].fetchDocument(self._key)
        except:
            colname = f"{type(self).__mro__[1].__name__}Col"
            return db[colname].fetchDocument(self._key)


class BaseVertex(BaseObject):
    def __init__(self, _id, _key, _rev):
        super().__init__(_id, _key, _rev)

    def upsert(self, db):
        self._upsert(db)

    def _upsert(self, db):
        try:
            colname = f"{type(self).__name__}Col"
            collection = db[colname]
        except:
            colname = f"{type(self).__mro__[1].__name__}Col"
            collection = db[colname]
        doc = collection.createDocument(self.__dict__)
        doc.save()
        self._update_privates(doc)


class BaseEdge(BaseObject):
    def __init__(self, _id, _key, _rev, _from, _to):
        super().__init__(_id, _key, _rev)
        self._from = _from
        self._to = _to

    def upsert(self, db, _from, _to):
        self._upsert(self, db, _from, _to)

    def _upsert(self, db, _from, _to):
        colname = f"{self.__class__.__name__}Col"
        collection = db[colname]
        doc = collection.createEdge(self.__dict__)
        doc.links(_from.to_document(db), _to.to_document(db))
        doc.save()
        self._update_privates(doc)


# VERTICES


class ReadableDatapoint(BaseVertex):
    def __init__(
        self,
        plant_code,
        device_code,
        code,
        type,
        name="",
        frequency=None,
        grouping_function="mean",
        _id=None,
        _key=None,
        _rev=None,
    ):
        super().__init__(_id, _key, _rev)

        self.name = name
        self.plant_code = plant_code
        self.device_code = device_code
        self.code = code
        self.frequency = frequency
        self.grouping_function = grouping_function
        self.type = type

    def upsert(self, db):
        colname = f"{self.__class__.__name__}Col"
        query = f"FOR dp IN {colname} FILTER dp.code == '{self.code}' AND dp.device_code == '{self.device_code}' AND dp.plant_code == '{self.plant_code}' RETURN dp"
        result = db.AQLQuery(query)

        if len(result) == 0:
            collection = db[colname]
            doc = collection.createDocument(self.__dict__)
            doc.save()
            self._update_privates(doc)
        else:
            doc = result[0]
            update_keys = [
                key
                for key in doc.getStore().keys()
                if key not in ["_id", "_key", "_rev"]
            ]
            for key in update_keys:
                doc[key] = self.__dict__[key]
            doc.patch()
            self._update_privates(doc)


class WritableDatapoint(BaseVertex):
    def __init__(
        self,
        plant_code,
        device_code,
        code,
        simonlab_id,
        name="",
        _id=None,
        _key=None,
        _rev=None,
    ):
        super().__init__(_id, _key, _rev)

        self.name = name
        self.plant_code = plant_code
        self.device_code = device_code
        self.code = code
        self.simonlab_id = simonlab_id

    def upsert(self, db):
        colname = f"{self.__class__.__name__}Col"
        query = f"FOR dp IN {colname} FILTER dp.code == '{self.code}' AND dp.device_code == '{self.device_code}' AND dp.plant_code == '{self.plant_code}' RETURN dp"
        result = db.AQLQuery(query)

        if len(result) == 0:
            collection = db[colname]
            doc = collection.createDocument(self.__dict__)
            doc.save()
            self._update_privates(doc)
        else:
            doc = result[0]
            update_keys = [
                key
                for key in doc.getStore().keys()
                if key not in ["_id", "_key", "_rev"]
            ]
            for key in update_keys:
                doc[key] = self.__dict__[key]
            doc.patch()
            self._update_privates(doc)


class Equip(BaseVertex):
    def __init__(self, name, type, device_code, _id=None, _key=None, _rev=None):
        super().__init__(_id, _key, _rev)

        self.name = name
        self.type = type
        self.device_code = device_code
        # self.specs = specs
        # self.schedule_dp = schedule_dp

    def specialize(type, **kwargs):
        if type in PARENT_EQUIPS:
            return ParentEquip(type=type, **kwargs)
        else:
            return ChildEquip(type=type, **kwargs)

    def get_sub_equips(self, db, min_depth=1, max_depth=5, filter=None):
        query = f"FOR sub_equip in {min_depth}..{max_depth} INBOUND '{self._id}' ComponentOfCol RETURN sub_equip"
        if filter is not None:
            key = list(filter.keys())[0]
            query = query.replace(
                "RETURN sub_equip",
                f"FILTER sub_equip.{key} == '{filter[key]}' RETURN sub_equip",
            )
        equips = db.AQLQuery(query, rawResults=True)
        return [ChildEquip(**equip) for equip in equips]

    def get_sensors(self, db, min_depth=0, max_depth=5, bound="IN", filter=None):
        query = f"""
        FOR equip in (
            FOR sub_equip in {min_depth}..{max_depth} {bound}BOUND '{self._id}' ComponentOfCol RETURN sub_equip._id
        )
        FOR sensor in 1..1 INBOUND equip ReadCol RETURN DISTINCT sensor
        """
        if filter is not None:
            key = list(filter.keys())[0]
            query = query.replace(
                "RETURN sub_equip._id",
                f"FILTER sub_equip.{key} == '{filter[key]}' RETURN sub_equip._id",
            )
        sensors = db.AQLQuery(query, rawResults=True)
        return [Sensor(**sensor) for sensor in sensors]

    def get_actions(self, db, min_depth=0, max_depth=5):
        query = f"""
        FOR equip in (
            FOR sub_equip in {min_depth}..{max_depth} INBOUND '{self._id}' ComponentOfCol RETURN sub_equip._id
        )
        FOR action in 1..1 OUTBOUND equip PerformCol RETURN DISTINCT action
        """
        actions = db.AQLQuery(query, rawResults=True)
        return [Action(**action) for action in actions]

    def implements_command(self, db, base_url, token, type, value):
        query = f"FOR v in 1..1 OUTBOUND '{self._id}' ImplementCol FILTER v.type == '{type}' RETURN v"
        command = db.AQLQuery(query, rawResults=True)
        command = Command(**command[0])
        command.apply(db, base_url, token, value)

    # TODO: questo va specializzato per ogni classe e devo ridefinire get_sensors perchè
    # per avere tutto uno ha la relazione outbound, l'altro inbound --> HO DECISO CHE SI FA LA RICERCA SOLO "SOTTO"
    def get_sensors_data_by_attribute(
        self,
        db,
        base_url,
        token,
        date_from=None,
        date_to=None,
        attribute="temperature",
        aggregation=None,
        recursive=True,
        filter=None,
    ):
        max_depth = 0 if not recursive else 5
        sensors = self.get_sensors(db, max_depth=max_depth, filter=filter)
        return _get_sensors_attribute_data(
            sensors, db, base_url, token, date_from, date_to, attribute, aggregation
        )

    def is_on(self, db, base_url=None, token=None, data=None):
        def get_state(data):
            sensors = self.get_sensors(db, max_depth=0)
            for sensor in sensors:
                dps = sensor.get_measurements(db, attribute="on-off")
                if len(dps) > 0:
                    return data.tail(1)[dps[0].name].item() == 1
            return None

        if data is None:
            date_from = datetime.utcnow() - timedelta(hours=1)
            data = self.get_sensors_data_by_attribute(
                db, base_url, token, date_from=date_from, attribute=None
            )
        return get_state(data)

    def is_off(self, db, base_url=None, token=None, data=None):
        def get_state(data):
            sensors = self.get_sensors(db, max_depth=0)
            for sensor in sensors:
                dps = sensor.get_measurements(db, attribute="on-off")
                if len(dps) > 0:
                    return data.tail(1)[dps[0].name].item() == 0
            return None

        if data is None:
            date_from = datetime.utcnow() - timedelta(hours=1)
            data = self.get_sensors_data_by_attribute(
                db, base_url, token, date_from=date_from, attribute=None
            )
        return get_state(data)


class ParentEquip(Equip):
    def __init__(self, name, type, device_code, _id=None, _key=None, _rev=None):
        super().__init__(name, type, device_code, _id, _key, _rev)

    def get_space(self, db):
        query = f"FOR space in 1..1 OUTBOUND '{self._id}' ServeCol RETURN space"
        space = db.AQLQuery(query, rawResults=True)
        return Space(**space[0])

    # def get_sensors(self, db, min_depth=0, max_depth=5):  # , edge_filter=None):
    #     return self._get_sensors(
    #         db, min_depth, max_depth, bound="IN"  # , edge_type=edge_filter
    #     )

    def get_daily_schedule(self, db, base_url, token):
        return self._get_schedule(db, base_url, token, "daily")

    def get_weekly_schedule(self, db, base_url, token):
        return self._get_schedule(db, base_url, token, "weekly")

    def _get_schedule(self, db, base_url, token, timeframe):
        if self.schedule_dp == "":
            return None

        dp = WritableDatapoint(
            **db["WritableDatapointCol"].fetchDocument(
                self.schedule_dp.split("/")[1], rawResults=True
            )
        )

        return get_schedulation(
            base_url=base_url,
            token=token,
            datapoint_id=dp.simonlab_id,
            timeframe=timeframe,
        )

    def check_comfort(
        self,
        db,
        base_url,
        token,
        season,
        color="red",
        attributes=None,
    ):
        space = self.get_space(db)

        if attributes is None:
            attributes = ["co2", "temperature", "humidity"]

        comfort_flag = True
        for attribute in attributes:
            date_from = datetime.utcnow() - timedelta(hours=1)
            data = self.get_sensors_data_by_attribute(
                db,
                base_url,
                token,
                date_from=date_from,
                attribute=attribute,
                aggregation="mean",
            )
            if data is None:
                continue

            cs = space.get_comfort_settings(db, attribute=attribute)
            if cs is None:
                continue

            params = cs.get_params(season=season)
            if color == "red":
                lower_limit, upper_limit = get_red_limit(params)
            elif color == "green":
                lower_limit, upper_limit = get_green_limit(params)
            else:
                lower_limit, upper_limit = get_yellow_limit(params)

            actual_value = data.tail(1)[attribute].item()
            comfort_flag = comfort_flag & (lower_limit <= actual_value <= upper_limit)
        return comfort_flag


class ChildEquip(Equip):
    def __init__(self, name, type, device_code, _id=None, _key=None, _rev=None):
        super().__init__(name, type, device_code, _id, _key, _rev)

    def get_parent_equip(self, db):
        query = f"FOR equip in 1..1 OUTBOUND '{self._id}' ComponentOfCol RETURN equip"
        equip = db.AQLQuery(query, rawResults=True)
        return ParentEquip(**equip[0])

    def get_space(self, db):
        parent_equip = self.get_parent_equip(db)
        return parent_equip.get_space(db)

    # def get_sensors(self, db, min_depth=0, max_depth=5):  # , edge_filter=None):
    #     return self._get_sensors(
    #         db, min_depth, max_depth, bound="OUT"  # , edge_type=edge_filter
    #     )


class Sensor(BaseVertex):
    def __init__(self, name, measurements, _id=None, _key=None, _rev=None):
        super().__init__(_id, _key, _rev)

        self.name = name
        self.measurements = measurements

    def get_space(self, db):
        query = f"FOR v in 1..1 OUTBOUND '{self._id}' MonitorCol RETURN v"
        results = db.AQLQuery(query, rawResults=True)
        return Space(**results[0])

    def _get_raw_data(self, base_url, token, dps, date_from, date_to):
        data = []
        for dp in dps:
            raw_data = _retrieve_data(
                base_url,
                token,
                dp.plant_code,
                dp.device_code,
                dp.code,
                date_from=date_from,
                date_to=date_to,
                alias=dp.name,
            )
            data.append(_clean_data(raw_data, dp.frequency, dp.grouping_function, None))
        data = pd.concat(data, axis=1)
        return data

    def get_measurements(self, db, attribute=None):
        dps = []
        for id in self.measurements:
            dp = ReadableDatapoint(
                **db["ReadableDatapointCol"].fetchDocument(
                    id.split("/")[1], rawResults=True
                )
            )
            if attribute is None or attribute == dp.type:
                dps.append(dp)
        return dps

    def get_data(
        self,
        db,
        base_url,
        token,
        date_from=None,
        date_to=None,
        attribute=None,
        # return_freq=False,
    ):
        dps = self.get_measurements(db, attribute)
        if len(dps) == 0:
            return None

        if date_to is None:
            date_to = datetime.utcnow()
        if date_from is None:
            date_from = date_to - timedelta(hours=168)
        date_to = calendar.timegm(date_to.utctimetuple())
        date_from = calendar.timegm(date_from.utctimetuple())

        data = self._get_raw_data(base_url, token, dps, date_from, date_to)

        # return all data
        if attribute is None:
            return data

        # return data filtered on specific type and aggregated
        agg_func = dps[0].grouping_function
        data = data.agg(agg_func, axis=1).to_frame()
        data.columns = [attribute]
        # if return_freq:
        #     return data, dps[0].frequency
        return data


class ComfortSettings(BaseVertex):
    def __init__(
        self,
        type,
        schedule_dp,
        params_summer,
        params_winter,
        name=None,
        _id=None,
        _key=None,
        _rev=None,
    ):
        super().__init__(_id, _key, _rev)

        self.name = name
        self.type = type
        self.params_summer = params_summer
        self.params_winter = params_winter
        self.schedule_dp = schedule_dp

    def get_params(self, season=None):
        if season is None:
            return {"summer": self.params_summer, "winter": self.params_winter}
        if season == "summer":
            return self.params_summer
        if season == "winter":
            return self.params_winter

    def get_daily_schedule(self, db, base_url, token):
        return self._get_sched(db, base_url, token, "daily")

    def get_weekly_schedule(self, db, base_url, token):
        return self._get_sched(db, base_url, token, "weekly")

    def _get_sched(self, db, base_url, token, timeframe):
        dp = WritableDatapoint(
            **db["WritableDatapointCol"].fetchDocument(
                self.schedule_dp.split("/")[1], rawResults=True
            )
        )

        return get_schedulation(
            base_url=base_url,
            token=token,
            datapoint_id=dp.simonlab_id,
            timeframe=timeframe,
        )

    def add_comfort_to_data(self, season, data, col, color="red"):
        col = data.columns[0] if col is None else col

        params = self.get_params(season)
        if color == "red":
            lower_limit, upper_limit = get_red_limit(params)
        elif color == "green":
            lower_limit, upper_limit = get_green_limit(params)
        else:
            lower_limit, upper_limit = get_yellow_limit(params)

        data[f"{self.type}_in_comfort"] = np.where(
            (data[col] > lower_limit) & (data[col] < upper_limit), 1, 0
        )
        return data


class Space(BaseVertex):
    def __init__(
        self,
        name,
        type,
        _id=None,
        _key=None,
        _rev=None,
        custom_settings=None,
    ):
        super().__init__(_id, _key, _rev)

        self.name = name
        self.type = type
        self.custom_settings = custom_settings

    def get_parent_space(self, db):
        query = f"FOR sub_space in 1..1 OUTBOUND '{self._id}' IsInCol RETURN sub_space"
        space = db.AQLQuery(query, rawResults=True)
        if len(space) > 0:
            return Site(**space[0]) if space[0]["type"] == "site" else Space(**space[0])
        return None

    def get_sub_spaces(self, db, min_depth=1, max_depth=5, filter=None):
        query = f"FOR sub_space in {min_depth}..{max_depth} INBOUND '{self._id}' IsInCol RETURN sub_space"
        if filter is not None:
            key = list(filter.keys())[0]
            query = query.replace(
                "RETURN sub_space",
                f"FILTER sub_space.{key} == '{filter[key]}' RETURN sub_space",
            )
        spaces = db.AQLQuery(query, rawResults=True)
        return [Space(**space) for space in spaces]

    # def get_comfort_settings(self, db, attribute):
    #     query = f"""
    #     LET comfort_settings = (FOR a IN ComfortSettingsCol FILTER a.type == '{attribute}' RETURN a)
    #     FOR s IN comfort_settings
    #         LET path_len = COUNT(FOR v IN any shortest_path '{self._id}' TO s._id GRAPH 'SimonlabGraph' RETURN v._id)
    #         FILTER path_len > 0
    #         RETURN {{'path_len': path_len, 'vertex': s}}
    #     """
    #     results = db.AQLQuery(query, rawResults=True)
    #     if len(results) == 0:
    #         return None
    #     cs = min(results, key=lambda x: x["path_len"])
    #     return ComfortSettings(**cs["vertex"])

    def get_comfort_settings(self, db, attribute):
        query = f"""
        FOR space in (
            FOR sub_space in 0..5 OUTBOUND '{self._id}' IsInCol RETURN sub_space._id
        )
        FOR cs in 1..1 INBOUND space ConstraintOfCol FILTER cs.type == '{attribute}' RETURN DISTINCT cs
        """
        cs = db.AQLQuery(query, rawResults=True)
        return ComfortSettings(**cs[0]) if len(cs) > 0 else None

    def get_equips(self, db, min_depth=0, max_depth=5):
        query = f"""
        FOR space in (
            FOR sub_space in {min_depth}..{max_depth} INBOUND '{self._id}' IsInCol RETURN sub_space._id
        )
        FOR equip in 1..1 INBOUND space ServeCol RETURN DISTINCT equip
        """
        equips = db.AQLQuery(query, rawResults=True)
        return [Equip.specialize(**equip) for equip in equips]

    def get_sensors(self, db, min_depth=0, max_depth=5, filter=None):
        query = f"""
        FOR space in (
            FOR sub_space in {min_depth}..{max_depth} INBOUND '{self._id}' IsInCol RETURN sub_space._id
        )
        FOR sensor in 1..1 INBOUND space MonitorCol RETURN DISTINCT sensor
        """
        if filter is not None:
            key = list(filter.keys())[0]
            query = query.replace(
                "RETURN sub_space._id",
                f"FILTER sub_space.{key} == '{filter[key]}' RETURN sub_space._id",
            )
        sensors = db.AQLQuery(query, rawResults=True)
        return [Sensor(**sensor) for sensor in sensors]

    def get_actions(
        self,
        db,
        min_space_depth=0,
        max_space_depth=5,
        min_equip_depth=0,
        max_equip_depth=5,
    ):
        # TODO: se 'max_equip_depth'=0, ritorna SOLO le action degli equip "padre"
        #       se 'min_equip_depth'=1, ritorna SOLO le action deglie equip "figli"
        #       coi valori di default, ritorna le actions di entrambi.
        # 'min_space_depth' e 'max_space_depth' conrollano quali space e sub-space considerare.
        query = f"""
        FOR sub_equip in (
            FOR equip in (
                FOR space in (
                    FOR sub_space in {min_space_depth}..{max_space_depth} INBOUND '{self._id}' IsInCol RETURN sub_space._id
                )
                FOR equip in 1..1 INBOUND space ServeCol RETURN equip._id
            )
            FOR sub_equip in {min_equip_depth}..{max_equip_depth} INBOUND equip ComponentOfCol RETURN sub_equip._id
        )
        FOR action in 1..1 OUTBOUND sub_equip PerformCol RETURN DISTINCT action
        """
        actions = db.AQLQuery(query, rawResults=True)
        return [Action(**action) for action in actions]

    def get_sensors_data_by_attribute(
        self,
        db,
        base_url,
        token,
        date_from=None,
        date_to=None,
        attribute="temperature",
        aggregation=None,
        recursive=True,
        filter=None,
    ):
        max_depth = 0 if not recursive else 5
        sensors = self.get_sensors(db, max_depth=max_depth, filter=filter)
        return _get_sensors_attribute_data(
            sensors, db, base_url, token, date_from, date_to, attribute, aggregation
        )


class Site(Space):
    def __init__(
        self,
        name,
        code,
        lat,
        lon,
        season_dp,
        watchdog_dp,
        outer_temperature,
        type="site",
        _id=None,
        _key=None,
        _rev=None,
        custom_settings=None,
    ):
        super().__init__(name, type, _id, _key, _rev, custom_settings)

        self.name = name
        self.code = code
        self.lat = lat
        self.lon = lon
        self.season_dp = season_dp
        self.watchdog_dp = watchdog_dp
        self.outer_temperature = outer_temperature

    def get_buildings(self, db):
        return self.get_sub_spaces(db, min_depth=1, max_depth=1)

    def get_season(self, db, base_url, token, formatted=True):
        dp = WritableDatapoint(
            **db["WritableDatapointCol"].fetchDocument(
                self.season_dp.split("/")[1], rawResults=True
            )
        )
        return read_writable_datapoint(
            base_url, token, dp.simonlab_id, formatted=formatted
        )

    def get_outer_temperature(self, db):
        return ReadableDatapoint(
            **db["ReadableDatapointCol"].fetchDocument(
                self.outer_temperature.split("/")[1], rawResults=True
            )
        )

    def _get_raw_data(self, base_url, token, dp, date_from, date_to):
        raw_data = _retrieve_data(
            base_url,
            token,
            dp.plant_code,
            dp.device_code,
            dp.code,
            date_from=date_from,
            date_to=date_to,
        )
        return _clean_data(raw_data, dp.frequency, dp.grouping_function, None)

    def get_outer_temperature_data(
        self, db, base_url, token, date_from=None, date_to=None
    ):
        dp = self.get_outer_temperature(db)

        if date_to is None:
            date_to = datetime.utcnow()
        if date_from is None:
            date_from = date_to - timedelta(hours=24)
        date_to = calendar.timegm(date_to.utctimetuple())
        date_from = calendar.timegm(date_from.utctimetuple())

        data = self._get_raw_data(base_url, token, dp, date_from, date_to)
        data.columns = ["outer_temperature"]
        return data

    def delete_buildings(self, db, max_depth=15):
        query = f"FOR v, e in 1..{max_depth} ANY '{self._id}' GRAPH 'SimonlabGraph' FILTER NOT LIKE(v._id, 'Algorithm%') RETURN DISTINCT v"
        vertices = db.AQLQuery(query)
        query = f"FOR v, e in 1..{max_depth} ANY '{self._id}' GRAPH 'SimonlabGraph' RETURN DISTINCT e"
        edges = db.AQLQuery(query)

        [e.delete() for e in edges]
        [v.delete() for v in vertices]


class Command(BaseVertex):
    def __init__(
        self,
        name,
        type,
        output_dp,
        output_type,
        output_specs,
        duration=3600,
        _id=None,
        _key=None,
        _rev=None,
    ):
        super().__init__(_id, _key, _rev)

        self.name = name
        # TODO: si potrebbe cambiare questo nome
        self.type = type
        self.output_dp = output_dp
        self.output_type = output_type
        self.output_specs = output_specs
        self.duration = duration

    def _compute_output(self, value):
        if self.output_type == "bool":
            return (
                float(self.output_specs["on_val"])
                if value == 1
                else float(self.output_specs["off_val"])
            )
        elif self.output_type == "range":
            return (
                float(self.output_specs["min_val"])
                + (
                    float(self.output_specs["max_val"])
                    - float(self.output_specs["min_val"])
                )
                * value
            )

    def apply(self, db, base_url, token, value):
        dp = WritableDatapoint(
            **db["WritableDatapointCol"].fetchDocument(
                self.output_dp.split("/")[1], rawResults=True
            )
        )
        url = urljoin(
            base_url, f"/writableDatapoints/{dp.simonlab_id}/commands/setPoint"
        )
        output = self._compute_output(value)
        # TODO: togli il commento
        # send_command_to_simon(url, token, output, self.duration)
        print(f"\tCommand: {self.name} ({dp.simonlab_id}) --> {output}")
        return output


class Action(BaseVertex):
    def __init__(
        self, name, type, when, algo_aggregation="mean", _id=None, _key=None, _rev=None
    ):
        super().__init__(_id, _key, _rev)

        self.name = name
        self.type = type
        self.when = when
        self.algo_aggregation = algo_aggregation

    def get_algorithms(self, db):
        query = f"FOR v in 1..1 INBOUND '{self._id}' DriveCol RETURN v"
        algorithms = db.AQLQuery(query, rawResults=True)
        return [Algorithm(**algo) for algo in algorithms]

    def get_equips(self, db):
        query = f"FOR v in 1..1 INBOUND '{self._id}' PerformCol RETURN v"
        equips = db.AQLQuery(query, rawResults=True)
        return [Equip.specialize(**equip) for equip in equips]

    def compute_command(self, algos_results, w=None):
        if self.algo_aggregation == "mean":
            return np.array(algos_results).mean()
        elif self.algo_aggregation == "weighted_mean":
            return np.average(algos_results, weights=w)
        elif self.algo_aggregation == "min":
            return np.array(algos_results).min()
        elif self.algo_aggregation == "max":
            return np.array(algos_results).max()
        elif self.algo_aggregation == "and":
            return np.array(algos_results).all()
        elif self.algo_aggregation == "or":
            return np.array(algos_results).any()


class Algorithm(BaseVertex):
    def __init__(
        self, uuid, name, function_name, priority=None, _id=None, _key=None, _rev=None
    ):
        super().__init__(_id, _key, _rev)

        self.uuid = uuid
        self.name = name
        self.priority = priority
        self.function_name = function_name

    def apply(self, db, **kwargs):
        try:
            # TODO: faccio la libreria degli algoritmi
            return getattr(algos_toolkit, self.function_name)(db, **kwargs)
        except Exception as e:
            raise ValueError(e)


# EDGES


class IsIn(BaseEdge):
    def __init__(self, _id=None, _key=None, _rev=None, _from=None, _to=None):
        super().__init__(_id, _key, _rev, _from, _to)

    def upsert(self, db, _from, _to):
        if (type(_from) == Space) and ((type(_to) == Space) or (type(_to) == Site)):
            self._upsert(db, _from, _to)
        else:
            raise ValueError(
                f"Wrong type: expected '{Space.__name__} -> {Space.__name__}', found: '{_from.__class__.__name__} -> {_to.__class__.__name__}'"
            )

    def __str__(self):
        return f"IsIn - {self._key}, {self._id}: {self._from} -> {self._to}"


class Monitor(BaseEdge):
    def __init__(self, _id=None, _key=None, _rev=None, _from=None, _to=None):
        super().__init__(_id, _key, _rev, _from, _to)

    def upsert(self, db, _from, _to):
        if (type(_from) == Sensor) and (type(_to) == Space):
            self._upsert(db, _from, _to)
        else:
            raise ValueError(
                f"Wrong type: expected '{Sensor.__name__} -> {Space.__name__}', found: '{_from.__class__.__name__} -> {_to.__class__.__name__}'"
            )


class Read(BaseEdge):
    def __init__(self, _id=None, _key=None, _rev=None, _from=None, _to=None):
        super().__init__(_id, _key, _rev, _from, _to)

    def upsert(self, db, _from, _to):
        if (type(_from) == Sensor) and (
            (type(_to) == ParentEquip) or (type(_to) == ChildEquip)
        ):
            self._upsert(db, _from, _to)
        else:
            raise ValueError(
                f"Wrong type: expected '{Sensor.__name__} -> {Equip.__name__}', found: '{_from.__class__.__name__} -> {_to.__class__.__name__}'"
            )


class Serve(BaseEdge):
    def __init__(self, _id=None, _key=None, _rev=None, _from=None, _to=None):
        super().__init__(_id, _key, _rev, _from, _to)

    def upsert(self, db, _from, _to):
        if ((type(_from) == ParentEquip) or (type(_from) == ChildEquip)) and (
            type(_to) == Space or isinstance(_to, Equip)
        ):
            self._upsert(db, _from, _to)
        else:
            raise ValueError(
                f"Wrong type: expected '{Equip.__name__} -> {Space.__name__}', found: '{_from.__class__.__name__} -> {_to.__class__.__name__}'"
            )


class ConstraintOf(BaseEdge):
    def __init__(self, _id=None, _key=None, _rev=None, _from=None, _to=None):
        super().__init__(_id, _key, _rev, _from, _to)

    def upsert(self, db, _from, _to):
        if (type(_from) == ComfortSettings) and (
            (type(_to) == Space) or (type(_to) == Site)
        ):
            self._upsert(db, _from, _to)
        else:
            raise ValueError(
                f"Wrong type: expected '{ComfortSettings.__name__} -> {Space.__name__}', found: '{_from.__class__.__name__} -> {_to.__class__.__name__}'"
            )


class ComponentOf(BaseEdge):
    def __init__(self, type=None, _id=None, _key=None, _rev=None, _from=None, _to=None):
        super().__init__(_id, _key, _rev, _from, _to)

        self.type = type

    def upsert(self, db, _from, _to):
        if ((type(_from) == ParentEquip) or (type(_from) == ChildEquip)) and (
            (type(_to) == ParentEquip) or (type(_to) == ChildEquip)
        ):
            self._upsert(db, _from, _to)
        else:
            raise ValueError(
                f"Wrong type: expected '{Equip.__name__} -> {Equip.__name__}', found: '{_from.__class__.__name__} -> {_to.__class__.__name__}'"
            )


class Implement(BaseEdge):
    def __init__(self, _id=None, _key=None, _rev=None, _from=None, _to=None):
        super().__init__(_id, _key, _rev, _from, _to)

    def upsert(self, db, _from, _to):
        if ((type(_from) == ParentEquip) or (type(_from) == ChildEquip)) and (
            type(_to) == Command
        ):
            self._upsert(db, _from, _to)
        else:
            raise ValueError(
                f"Wrong type: expected '{Equip.__name__} -> {Command.__name__}', found: '{_from.__class__.__name__} -> {_to.__class__.__name__}'"
            )


class Perform(BaseEdge):
    def __init__(self, _id=None, _key=None, _rev=None, _from=None, _to=None):
        super().__init__(_id, _key, _rev, _from, _to)

    def upsert(self, db, _from, _to):
        if ((type(_from) == ParentEquip) or (type(_from) == ChildEquip)) and (
            type(_to) == Action
        ):
            self._upsert(db, _from, _to)
        else:
            raise ValueError(
                f"Wrong type: expected '{Equip.__name__} -> {Action.__name__}', found: '{_from.__class__.__name__} -> {_to.__class__.__name__}'"
            )


class Drive(BaseEdge):
    def __init__(self, _id=None, _key=None, _rev=None, _from=None, _to=None):
        super().__init__(_id, _key, _rev, _from, _to)

    def upsert(self, db, _from, _to):
        if (type(_from) == Algorithm) and (type(_to) == Action):
            self._upsert(db, _from, _to)
        else:
            raise ValueError(
                f"Wrong type: expected '{Algorithm.__name__} -> {Action.__name__}', found: '{_from.__class__.__name__} -> {_to.__class__.__name__}'"
            )


# UTILS


def _get_sensors_attribute_data(
    sensors,
    db,
    base_url,
    token,
    date_from=None,
    date_to=None,
    attribute=None,
    aggregation=None,
):
    data = []
    for sensor in sensors:
        sensor_data = sensor.get_data(
            db, base_url, token, date_from, date_to, attribute
        )
        if sensor_data is not None:
            data.append(
                sensor_data
                # sensor_data.rename(
                #     columns={
                #         f"{attribute}": f"{attribute}_{sensor.name.replace(' ', '_')}".lower()
                #     }
                # )
            )
    if len(data) == 0:
        return None

    data = pd.concat(data, axis=1)
    if aggregation is not None:
        data = data.agg(aggregation, axis=1).to_frame()
        data.columns = [attribute]
    return data


def get_green_limit(comfort_params):
    return (
        float(comfort_params["min"]) + float(comfort_params["tol"]),
        float(comfort_params["max"]) - float(comfort_params["tol"]),
    )


def get_yellow_limit(comfort_params):
    return (
        float(comfort_params["min"]),
        float(comfort_params["max"]),
    )


def get_red_limit(comfort_params):
    return (
        float(comfort_params["min"]) - float(comfort_params["tol"]),
        float(comfort_params["max"]) + float(comfort_params["tol"]),
    )
