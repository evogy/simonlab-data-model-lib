import logging

from .push_edges import *
from .push_vertices import *


logger = logging.getLogger(__name__)


def validate_config_file(config_file):
    mandatory_keys = ["spaces", "equips", "comfort_settings", "sensors", "readable_datapoints"]
    for k in mandatory_keys:
        if k not in  config_file.keys():
            raise ValueError(f"Missing key {k} in config file")

    uuids = []
    for k, elements in config_file.items():
        if k in ["spaces", "equips", "readable_datapoints", "writable_datapoints"]:
            for v in elements:
                uuids.append(v["uuid"])
    if len(uuids) > len(set(uuids)):
        import collections
        duplicates =[item for item, count in collections.Counter(uuids).items() if count > 1]
        raise ValueError(f"Duplicated uuids {duplicates}")
    logger.info("Config file is valid.")


def push_spaces(db, wdps, rdps, spaces_config):
    edges = []
    vertices = {}
    for space_config in spaces_config:
        uuid = space_config.pop("uuid")
        link_to = (
            space_config.pop("is_in") if space_config.get("is_in") is not None else None
        )
        vertices[uuid] = push_space(db, space_config, wdps, rdps)

        if link_to is not None:
            edges.append(push_is_in(db, {"from": uuid, "to": link_to}, vertices))
    return vertices, edges


def push_equips(db, wdps, rdps, spaces, equips_config):
    def push_sub_equips(db, rdps, equip_config, vertices):
        links = []
        subequips = equip_config.pop("children", [])
        for subequip in subequips:
            subequip_name = (
                equip_config["name"] + " - " + subequip["type"].replace("_", " ")
            )
            config = {
                "name": subequip_name,
                "type": subequip["type"],
                "device_code": equip_config["device_code"],
            }
            vertices[subequip["uuid"]] = push_equip(db, config, wdps)
            links.append(subequip["uuid"])

            if subequip.get("measurements") is not None:
                sensors_config = {
                    "name": "Sensore " + subequip_name,
                    "read": subequip["uuid"],
                    "measurements": subequip["measurements"],
                }
                push_sensors(db, rdps, spaces, vertices, [sensors_config])

        links = None if len(links) == 0 else links
        return vertices, links

    equips_config = {e.pop("uuid"): e for e in equips_config}
    edges_to_space, edges_to_equip = [], []
    vertices = {}
    for uuid, equip_config in equips_config.items():
        # Create equips
        vertices, children_equips = push_sub_equips(db, rdps, equip_config, vertices)
        vertices[uuid] = push_equip(db,
                                    {k: v for k, v in equip_config.items() if k in ["name", "type", "device_code"]},
                                    wdps)

        # Create relations between parent equips and sub-equips
        if children_equips is not None:
            for child in children_equips:
                edges_to_equip.append(
                    push_component_of(db, {"from": child, "to": uuid}, vertices)
                )

    for uuid, equip_config in equips_config.items():
        # Create relations between equip and spaces or other equips
        links = (equip_config.pop("serve", None))
        if type(links) == str:
            links = [links]
        measurements = (equip_config.pop("measurements", None))

        if measurements is not None:
            # Create equip sensor
            sensors_config = {
                "name": "Sensore " + equip_config["name"],
                "read": uuid,
                "measurements": measurements,
            }
            push_sensors(db, rdps, spaces, vertices, [sensors_config])
            # Create spaces sensors
            for link in links:
                if link in spaces.keys():
                    sensors_config = {
                        "name": "Sensore " + equip_config["name"],
                        "measurements": measurements,
                        "monitor": link,
                    }
                    push_sensors(db, rdps, spaces, vertices, [sensors_config])

        if links is not None:
            for link in links:
                if link in spaces.keys():
                    edges_to_space.append(
                        push_serve(db, {"from": uuid, "to": link}, vertices, spaces)
                    )
                elif link in equips_config.keys():
                    edges_to_equip.append(
                        push_serve(db, {"from": uuid, "to": link}, vertices, vertices)
                    )
                else:
                    raise ValueError(f"Wrong type of vertex: {link}")

    return vertices, edges_to_equip, edges_to_space


def push_comfort_settings(db, wdps, spaces, comfort_settings_config):
    edges = []
    vertices = {}
    for cs_config in comfort_settings_config:
        uuid = cs_config.get("name")
        link_to = (
            cs_config.pop("constraint_of")
            if cs_config.get("constraint_of") is not None
            else None
        )
        vertices[uuid] = push_comfort_setting(db, cs_config, wdps)

        if link_to is not None:
            edges.append(
                push_constraint_of(db, {"from": uuid, "to": link_to}, vertices, spaces)
            )
    return vertices, edges


def push_sensors(db, rdps, spaces, equips, sensors_config):
    edges_to_space, edges_to_equip = [], []
    vertices = {}
    for sensor_config in sensors_config:
        uuid = sensor_config.get("name")
        link_to_space = (sensor_config.pop("monitor", None))
        link_to_equip = (sensor_config.pop("read", None))
        vertices[uuid] = push_sensor(db, sensor_config, rdps)

        if link_to_space is not None:
            edges_to_space.append(
                push_monitor(db, {"from": uuid, "to": link_to_space}, vertices, spaces)
            )
        if link_to_equip is not None:
            edges_to_equip.append(
                push_read(db, {"from": uuid, "to": link_to_equip}, vertices, equips)
            )
    return vertices, edges_to_space, edges_to_equip


def push_commands(db, wdps, equips, commands_config):
    edges = []
    vertices = {}
    for command_config in commands_config:
        uuid = command_config.get("name")
        link_to = (
            command_config.pop("implemented_by")
            if command_config.get("implemented_by") is not None
            else None
        )
        vertices[uuid] = push_command(db, command_config, wdps)

        if link_to is not None:
            edges.append(
                push_implement(db, {"from": link_to, "to": uuid}, equips, vertices)
            )
    return vertices, edges


def push_actions(db, equips, actions_config):
    edges_to_algo, edges_to_equip = [], []
    vertices = {}
    for action_config in actions_config:
        uuid = action_config.get("name")
        links_to_algo = (
            action_config.pop("algo") if action_config.get("algo") is not None else None
        )
        links_to_equip = (
            action_config.pop("performed_by")
            if action_config.get("performed_by") is not None
            else None
        )
        vertices[uuid] = push_action(db, action_config)

        if links_to_algo is not None:
            for link_to in links_to_algo:
                edges_to_algo.append(
                    push_drive(db, {"from": link_to, "to": uuid}, vertices)
                )
        if links_to_equip is not None:
            for link_to in links_to_equip:
                edges_to_equip.append(
                    push_perform(db, {"from": link_to, "to": uuid}, equips, vertices)
                )
    return vertices, edges_to_algo, edges_to_equip
