from . import model as m
from .utils import get_writable_datapoint_by_code
if __package__ is None or __package__ == '':
    from .. import db as db_module
else:
    import db as db_module

def push_writable_datapoint(
    db, dp_config, plant_code, base_url=None, token=None, uuid=None
):
    dp_config["name"] = uuid if dp_config.get("name") is None else dp_config["name"]

    dp_config["plant_code"] = (
        str(dp_config["plant_code"])
        if dp_config.get("plant_code") is not None
        else plant_code
    )

    if dp_config.get("simonlab_id") is None:
        if (base_url is not None) and (token is not None):
            dp_config["simonlab_id"] = get_writable_datapoint_by_code(
                base_url,
                token,
                dp_config["plant_code"],
                dp_config["device_code"],
                dp_config["code"],
            )["_id"]

    dp = m.WritableDatapoint(**dp_config)
    dp.upsert(db)
    return dp


def push_command(db, command_config, dps):
    command_config["output_dp"] = dps[command_config["output_dp"]]._id

    command = m.Command(**command_config)
    command.upsert(db)
    return command


def push_readable_datapoint(db, dp_config, plant_code, frequency, uuid=None):
    dp_config["name"] = uuid if dp_config.get("name") is None else dp_config["name"]
    dp_config["plant_code"] = (
        str(dp_config["plant_code"])
        if dp_config.get("plant_code") is not None
        else plant_code
    )
    dp_config["grouping_function"] = (
        "sum"
        if (dp_config["type"] == "energy" or dp_config["type"] == "baseline")
        else "mean"
    )
    dp_config["frequency"] = (
        str(dp_config["frequency"])
        if dp_config.get("frequency") is not None
        else frequency
    )
    if dp_config.get("alarm_rules") is not None:
        dp_config.pop("alarm_rules")

    dp = m.ReadableDatapoint(**dp_config)
    dp.upsert(db)
    return dp


def push_sensor(db, sensor_config, dps):
    sensor_config["measurements"] = [
        dps[dp]._id for dp in sensor_config["measurements"]
    ]

    sensor = m.Sensor(**sensor_config)
    sensor.upsert(db)
    return sensor


def push_comfort_setting(db, cs_config, dps):
    cs_config["schedule_dp"] = dps[cs_config["schedule_dp"]]._id

    if cs_config.get("params"):
        params = cs_config.pop("params")
        cs_config["params_winter"] = params
        cs_config["params_summer"] = params
    if cs_config.get("alarm_rules") is not None:
        cs_config.pop("alarm_rules")

    cs = m.ComfortSettings(**cs_config)
    cs.upsert(db)
    return cs


def push_equip(db, equip_config, dps):
    if equip_config["type"] in db_module.PARENT_EQUIPS:
        # equip_config["schedule_dp"] = dps[equip_config["schedule_dp"]]._id
        equip = m.ParentEquip(**equip_config)
    else:
        equip = m.ChildEquip(**equip_config)
    equip.upsert(db)
    return equip


def push_space(db, space_config, wdps, rdps):
    if space_config["type"] == "site":
        space_config["season_dp"] = wdps[space_config["season_dp"]]._id
        space_config["watchdog_dp"] = wdps[space_config["watchdog_dp"]]._id
        space_config["outer_temperature"] = rdps[space_config["outer_temperature"]]._id
        space = m.Site(**space_config)
    else:
        space = m.Space(**space_config)
    space.upsert(db)
    return space


def push_action(db, action_config):
    action = m.Action(**action_config)
    action.upsert(db)
    return action


# def push_algorithm(db, algorithm_config):
#     algorithm = m.Algorithm(**algorithm_config)
#     algorithm.upsert(db)
#     return algorithm
