import pytz
import json
import yaml
import requests
import pandas as pd
from urllib.parse import urljoin
from datetime import datetime, timedelta

# LOAD CONFIG FILE


def load_config_file(path):
    with open(path, "r") as f:
        conf = yaml.load(f, Loader=yaml.FullLoader)
    return conf


# GET SCHED


def _request_datapoint_by_code(url, header):
    req = requests.get(url, headers=header)
    if req.status_code == 200:
        if len(req.json()) == 1:
            return req.json()[0]
    return None


def get_writable_datapoint_by_code(
    base_url, token, plant_code, device_code, datapoint_code
):
    header = {"Authorization": "Bearer " + token}
    query = f"?plantCode={plant_code}&deviceCode={device_code}&code={datapoint_code}"
    url = urljoin(base_url, "/writableDatapoints" + query)
    return _request_datapoint_by_code(url, header)


# GET DATA


def _clean_data(data, group_freq, group_fn, resampling_fn):
    group_fn = group_fn if group_fn is not None else "mean"
    resampling_fn = resampling_fn if resampling_fn is not None else "linear"

    data.index = data.index.tz_convert("Europe/Rome")
    data = data.resample(group_freq).apply(group_fn)
    data = data.interpolate(method=resampling_fn, limit_direction="both")
    data.index = data.index.tz_convert("UTC")
    return data


def _retrieve_data(
    base_url,
    token,
    plant_code,
    device_code,
    datapoint_code,
    date_from,
    date_to,
    alias=None,
):
    header = {"Authorization": "Bearer " + token}
    url = urljoin(
        base_url,
        f"datapoint/raw/{datapoint_code}/{device_code}/{plant_code}/{date_from}/{date_to}",
    )
    req = requests.get(url, headers=header)

    if req.status_code == 200:
        data = json.loads(req._content.decode("utf-8"))
        data = pd.DataFrame(
            data["series"][0]["values"], columns=data["series"][0]["columns"]
        )
        data = data.set_index("time")
        data.index = pd.to_datetime(data.index)

        if alias is not None:
            data.columns = [alias]

        return data
    else:
        print(f"Error occurred during retrieve_data API with error: {req.text}")
        raise requests.RequestException("Failed to retrieve data.")


# GET SCHED


def _get_sched(schedulations, utc_now):
    def parse_date(date_str: str):
        date = datetime.fromisoformat(date_str.replace("Z", ""))
        date_utc = pytz.timezone("UTC").localize(date)
        date_utc_as_eu_rome = date_utc.astimezone(pytz.timezone("Europe/Rome"))
        return date_utc_as_eu_rome

    def parse_profile_date(date_str: str, utc_now: datetime):
        date = datetime.fromisoformat(date_str.replace("Z", ""))
        date = date.replace(year=utc_now.year, month=utc_now.month, day=utc_now.day)
        date_eu_rome = pytz.timezone("Europe/Rome").localize(date)
        date_eu_rome_as_utc = date_eu_rome.astimezone(pytz.timezone("UTC"))
        return date_eu_rome_as_utc

    def get_schedule_time(entities: dict, utc_now: datetime):
        schedule_time = []
        weekday = utc_now.strftime("%A").lower()
        for sched in entities["weeklySetpoints"][weekday]:
            if sched["value"] != entities["defaultValue"]:
                start_utc = parse_profile_date(sched["start"], utc_now)
                end_utc = parse_profile_date(sched["end"], utc_now)
                if start_utc >= end_utc:
                    end_utc = end_utc + timedelta(days=1)
                schedule_time.append(
                    {
                        "start": start_utc,
                        "end": end_utc,
                        "value": sched["value"],
                    }
                )
        return {
            "default": entities["defaultValue"],
            "today": schedule_time if len(schedule_time) > 0 else None,
        }

    def date_is_in_range(date: datetime, start: str, end: str):
        start_utc = parse_date(start)
        end_utc = parse_date(end)
        return start_utc.date() <= date.date() <= end_utc.date()

    def date_is_in_wildcard(date: datetime, day: int, month: int):
        return (date.day == day) and (date.month == month)

    need_daily_sched = True
    daily_sched = None

    for schedulation in schedulations:
        if schedulation["schedulationType"] == "SpecialEvent":
            if schedulation["dateType"] == "BetweenTwoDates":
                if date_is_in_range(
                    utc_now, schedulation["start"], schedulation["end"]
                ):
                    need_daily_sched = False
            elif schedulation["dateType"] == "Wildcards":
                if date_is_in_wildcard(
                    utc_now, schedulation["anyDay"], schedulation["anyMonth"]
                ):
                    need_daily_sched = False

    for i, schedulation in enumerate(schedulations):
        if schedulation["schedulationType"] == "Profile":
            if schedulation["isDefault"] == False:
                if date_is_in_range(
                    utc_now, schedulation["start"], schedulation["end"]
                ):
                    if need_daily_sched:
                        daily_sched = get_schedule_time(
                            schedulation["linkedEntity"],
                            utc_now,
                        )
                    else:
                        daily_sched = {
                            "default": schedulation["linkedEntity"]["defaultValue"],
                            "today": None,
                        }
            else:
                default_profile_idx = i

    if daily_sched is None:
        if need_daily_sched:
            daily_sched = get_schedule_time(
                schedulations[default_profile_idx]["linkedEntity"],
                utc_now,
            )
        else:
            daily_sched = {
                "default": schedulations[default_profile_idx]["linkedEntity"][
                    "defaultValue"
                ],
                "today": None,
            }
    return daily_sched


def get_schedulation(
    base_url, token, datapoint_id, timeframe="daily", period="previous", offset=2
):
    header = {"Authorization": "Bearer " + token}
    url = urljoin(base_url, f"writableDatapoints/{datapoint_id}/entitySchedulations/")
    req = requests.get(url, headers=header)

    if req.status_code == 200:
        if len(req.json()) == 0:
            return None

        now_utc = pytz.timezone("UTC").localize(datetime.utcnow()) + timedelta(
            hours=int(offset)
        )

        if timeframe == "daily":
            sched = _get_sched(req.json(), now_utc)
            return sched

        if timeframe == "weekly":
            scheds = {}
            for i in range(1, 8):
                if period == "previous":
                    weekday = now_utc - timedelta(days=i)
                elif period == "next":
                    weekday = now_utc + timedelta(days=i)
                daily_sched = _get_sched(req.json(), weekday)
                scheds[weekday.strftime("%A").lower()] = daily_sched["today"]
            scheds["default"] = daily_sched["default"]
            return scheds
    else:
        print(f"Error occurred during get_schedulation API with error: \n {req.text}")
        return None


# READ SEASON


def read_writable_datapoint(base_url, token, datapoint_id, formatted=False):
    header = {"Authorization": "Bearer " + token}
    url = urljoin(base_url, f"/writableDatapoints/{datapoint_id}")
    req = requests.get(url, headers=header)

    if req.status_code == 200:
        if formatted:
            return req.json()["lastValueFormatted"]
        else:
            return req.json()["lastValue"]


# SEND COMMAND


def send_command_to_simon(url, token, action, duration=3660):
    body = {"value": {action}, "validityTime": duration}
    req = requests.post(url, data=body, headers={"Authorization": f"Bearer {token}"})
    if req.status_code != 200:
        print("Something goes wrong sending a new command to Simon")
