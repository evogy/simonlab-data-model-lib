from . import model as m


def push_is_in(db, link, spaces):
    edge = m.IsIn()
    edge.upsert(db, spaces[link["from"]], spaces[link["to"]])
    return edge


def push_monitor(db, link, sensors, spaces):
    edge = m.Monitor()
    edge.upsert(db, sensors[link["from"]], spaces[link["to"]])
    return edge


def push_read(db, link, sensors, equips):
    edge = m.Read()
    edge.upsert(db, sensors[link["from"]], equips[link["to"]])
    return edge


def push_serve(db, link, equips, targets):
    edge = m.Serve()
    edge.upsert(db, equips[link["from"]], targets[link["to"]])
    return edge


def push_constraint_of(db, link, comforts, spaces):
    edge = m.ConstraintOf()
    edge.upsert(db, comforts[link["from"]], spaces[link["to"]])
    return edge


def push_component_of(db, link, equips):  # , edge_type):
    edge = m.ComponentOf()  # type=edge_type)
    edge.upsert(db, equips[link["from"]], equips[link["to"]])
    return edge


def push_implement(db, link, equips, commands):
    edge = m.Implement()
    edge.upsert(db, equips[link["from"]], commands[link["to"]])
    return edge


def push_perform(db, link, equips, actions):
    edge = m.Perform()
    edge.upsert(db, equips[link["from"]], actions[link["to"]])
    return edge


def push_drive(db, link, actions):
    query = {"uuid": link["from"]}
    algo_dict = db["AlgorithmCol"].fetchFirstExample(query, rawResults=True)
    algo = m.Algorithm(**algo_dict[0])

    edge = m.Drive()
    edge.upsert(db, algo, actions[link["to"]])
    return edge
