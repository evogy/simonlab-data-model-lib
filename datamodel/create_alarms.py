import os
import sys
import argparse
from dotenv import load_dotenv, find_dotenv

from modeling import load_config_file
from ecologiclib import create_alarm_if_not_exists, update_alarm_if_exists

load_dotenv(find_dotenv())


def find_dps(attribute, dps):
    return [dp for dp in dps if dp["type"] == attribute]


def _build_alarm_info(
    plant_code, cs, rule, dp, default_mailing_list=None, add_uuid=True
):
    alarm_operator = ">" if cs["type"] == "co2" else "><"
    alarm_name = (
        rule.get("alarm_name")
        if rule.get("alarm_name") is not None
        else f"{cs['type'].capitalize()} fuori comfort"
    )
    if add_uuid:
        alarm_name = alarm_name + f" - {dp['uuid'].replace('_',' ').capitalize()}"

    alarm_check_interval = (
        rule.get("alarm_check_interval")
        if rule.get("alarm_check_interval") is not None
        else 0
    )

    alarm_info = {
        "plant_code": plant_code,
        "device_code": dp["device_code"],
        "dp_code": dp["code"],
        "alarm_name": alarm_name,
        "alarm_min": rule["limit_1"],
        "alarm_operator": alarm_operator,
        "alarm_check_interval": alarm_check_interval,
    }
    if rule.get("limit_2") is not None:
        alarm_info["alarm_max"] = rule["limit_2"]
    if rule.get("alarm_operator") is not None:
        alarm_info["alarm_operator"] = rule["alarm_operator"]
    if rule.get("alarm_level") is not None:
        alarm_info["alarm_level"] = rule["alarm_level"]
    if rule.get("alarm_dead_band") is not None:
        alarm_info["alarm_dead_band"] = rule["alarm_dead_band"]
    if rule.get("alarm_strikes") is not None:
        alarm_info["alarm_strikes"] = rule["alarm_strikes"]
    if rule.get("mailing_list") is not None:
        alarm_info["alarm_target_user_ids"] = rule["mailing_list"]
    else:
        alarm_info["alarm_target_user_ids"] = default_mailing_list
    return alarm_info


def create_alarms(config_file, base_url, token, create=True, update=False):
    print("ALARM RULES")
    print("  COMFORT SETTINGS:")

    plant_code = config_file["default_parameter"]["plant_code"]
    default_mailing_list = config_file["default_parameter"]["mailing_list"]
    comfort_settings = config_file["comfort_settings"]
    readable_datapoints = config_file["readable_datapoints"]

    for cs in comfort_settings:
        if cs.get("alarm_rules") is not None:
            dps = find_dps(cs["type"], readable_datapoints)

            for rule in cs.get("alarm_rules"):
                for dp in dps:
                    alarm_info = _build_alarm_info(
                        plant_code, cs, rule, dp, default_mailing_list
                    )
                    if create:
                        alarm = create_alarm_if_not_exists(base_url, token, alarm_info)
                        if alarm is None:
                            print(
                                f"""Impossibile creare la regola di allarme [ cs->{cs['type']} ] 
                                per il datapoint [ {plant_code} - {dp['device_code']} - {dp['code']} ]"""
                            )
                        else:
                            print(f"   - rule: {alarm['id']}")
                    if update:
                        alarm = update_alarm_if_exists(base_url, token, alarm_info)
                        if alarm is None:
                            print(
                                f"""Impossibile aggiornare la regola di allarme [ cs->{cs['type']} ]
                                per il datapoint [ {plant_code} - {dp['device_code']} - {dp['code']} ]"""
                            )
                        else:
                            print(f"  - rule: {alarm['id']}")

    print("\n  READABLE DATAPOINTS:")
    for dp in readable_datapoints:
        if dp.get("alarm_rules") is not None:
            for rule in dp.get("alarm_rules"):
                alarm_info = _build_alarm_info(
                    plant_code,
                    {"type": dp["type"]},
                    rule,
                    dp,
                    default_mailing_list,
                    False,
                )
                if create:
                    alarm = create_alarm_if_not_exists(base_url, token, alarm_info)
                    if alarm is None:
                        print(
                            f"""Impossibile creare la regola di allarme [ dp->{dp['type']} ] 
                            per il datapoint [ {plant_code} - {dp['device_code']} - {dp['code']} ]"""
                        )
                    else:
                        print(f"   - rule: {alarm['id']}")
                if update:
                    alarm = update_alarm_if_exists(base_url, token, alarm_info)
                    if alarm is None:
                        print(
                            f"""Impossibile aggiornare la regola di allarme [ dp->{dp['type']} ]
                            per il datapoint [ {plant_code} - {dp['device_code']} - {dp['code']} ]"""
                        )
                    else:
                        print(f"   - rule: {alarm['id']}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-cp", "--config_path", type=str, default="config_files/montecatini/mot.yml"
    )
    parser.add_argument("--dev", action="store_true")
    parser.add_argument("-c", "--create", action="store_true")
    parser.add_argument("-u", "--update", action="store_true")
    args = parser.parse_args()

    if args.dev:
        SIMONLAB_BASE_URL = os.getenv("SIMONLAB_BASE_URL_DEV")
        SIMONLAB_TOKEN = os.getenv("SIMONLAB_TOKEN_DEV")
    else:
        SIMONLAB_BASE_URL = os.getenv("SIMONLAB_BASE_URL")
        SIMONLAB_TOKEN = os.getenv("SIMONLAB_TOKEN")

    config_file = load_config_file(args.config_path)

    if args.create and args.update:
        print(
            "ERROR: Only one flag between '--create' an '--update' can be set to True"
        )
        sys.exit()

    create_alarms(
        config_file, SIMONLAB_BASE_URL, SIMONLAB_TOKEN, args.create, args.update
    )
